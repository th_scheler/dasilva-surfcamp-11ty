const htmlmin = require('html-minifier')
const generateImages = require('./src/short-codes/images')
const renderRelated = require('./src/short-codes/related')
const renderGallery = require('./src/short-codes/gallery')
const render2Columns = require('./src/short-codes/2-columns')
const renderBookingTeasers = require('./src/short-codes/booking-teasers')
const renderAddressMap = require('./src/short-codes/address-map')
const renderSurroundingsCards = require('./src/short-codes/surroundings-cards')
// const headingAnker = require('./src/plugins/heading-anker')

module.exports = function (eleventyConfig) {

  eleventyConfig.addPassthroughCopy({
    'src/assets': '_assets/',
  })

  eleventyConfig.addTransform('htmlmin', (content, outputPath) => {
    if (outputPath.endsWith('.html')) {
      return htmlmin.minify(content, {
        useShortDoctype: true,
        removeComments: true,
        collapseWhitespace: true,
        // continueOnParseError: true,
        minifyJS: true
      })
    }
    return content
  })

  // override annoying md code formatting
  const markdownIt = require('markdown-it')({
    html: true
  })
    .disable('code')
  // .use(require('markdown-it-github-headings'))
  eleventyConfig.setLibrary('md', markdownIt)

  // eleventyConfig.addPlugin(headingAnker)

  eleventyConfig.addNunjucksAsyncShortcode('image', ({ src, alt, myClass }) =>
    generateImages({ src, alt, myClass }))
  eleventyConfig.addNunjucksAsyncShortcode('hero', ({ src, alt, myClass }) =>
    generateImages({ src, alt, myClass, variant: 'hero' }))
  eleventyConfig.addNunjucksAsyncShortcode('related', ({ links, pages }) =>
    renderRelated({ links, pages }))
  eleventyConfig.addNunjucksAsyncShortcode("gallery", ({ images, altDefault, maxDisplayItems }) => 
    renderGallery({ images, altDefault, maxDisplayItems }))
  eleventyConfig.addNunjucksAsyncShortcode("2columns", ({ headline, subHeadline, leftGallery, rightText }) => 
    render2Columns({ headline, subHeadline, leftGallery, rightText }))
  eleventyConfig.addNunjucksAsyncShortcode("bookingTeasers", ({ links, teasers }) => 
    renderBookingTeasers({ links, teasers }))
  eleventyConfig.addNunjucksAsyncShortcode("addressMap", ({ address, mapSrc }) => 
    renderAddressMap({ address, mapSrc }))
  eleventyConfig.addNunjucksAsyncShortcode("surroundingsCards", ({ cards }) => 
    renderSurroundingsCards({ cards }))

  eleventyConfig.setBrowserSyncConfig({
    files: './dist/css/**/*.css'
  })

  return {
    dir: {
      input: 'src',
      layouts: 'templates',
      includes: 'templates',
      data: 'data',
      output: 'dist'
    },
    passthroughFileCopy: true,
    templateFormats: ['njk', 'md', 'css', 'html'],
    htmlTemplateEngine: 'njk',
    markdownTemplateEngine: 'njk'
  }
}
