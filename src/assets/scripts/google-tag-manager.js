
const tagManagerScriptHead = document.createElement('script')
tagManagerScriptHead.innerHTML = `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-53FHDSRP');`
const headElelment = document.getElementsByTagName('head')[0];
headElelment.appendChild(tagManagerScriptHead);

const tagManagerScriptBody = document.createElement('iframe');
tagManagerScriptBody.setAttribute('src', 'https://www.googletagmanager.com/ns.html?id=GTM-53FHDSRP');
tagManagerScriptBody.setAttribute('height', '0');
tagManagerScriptBody.setAttribute('width', '0');
tagManagerScriptBody.setAttribute('style', 'display:none;visibility:hidden');
document.body.appendChild(tagManagerScriptBody);

