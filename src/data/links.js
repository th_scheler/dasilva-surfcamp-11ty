const {readdir, stat, readFile} = require('node:fs/promises')
const path = require('path')
const yamlFront = require('yaml-front-matter')

const pagesPath = 'src/pages'

module.exports = async () => {
  const dirList = await getDirList(pagesPath)
  const partialsPathlist = getPartialsPathList(dirList)

  const partialContents = await readContents(partialsPathlist)
  // console.log('partialContents', JSON.stringify(partialContents, ' ', 2))

  const pages = partialContents.filter(partial => partial.permalink)
  const pagesPerLanguage = countPagesPerLanguage(pages)
  const languages = Object.keys(pagesPerLanguage)
  const links = createLinksJson(pages, languages)
  // check permalink, locale, tags, navigation, title
  checkHealth(links, pagesPerLanguage, languages)
  console.log(links)
  return links
}


function checkHealth(links, pagesPerLanguage) {
  console.log('CHECK HEALTH OF PAGES')
  console.log('Pages per language', pagesPerLanguage)
  // show which are missing

  // Check if some tags are different from the German tags
  const germanTags = Object.keys(links.de).sort()
  Object.keys(links).map(locale => Object.keys(links[locale]).sort())
  //[animals, bedAndBreakfast...],[animals, bedAndBreakfast...],...
      // compare each with the german tags
    .map(languageTags => germanTags.filter(tag => languageTags.indexOf(tag) === -1))
    .filter(differences => differences.length)
    .map(differences => console.error('WARNING! These tags are not consistent:', differences, 
      '\nChange this:\ntags:', differences[0]))
    // process.exit(1)
}

function createLinksJson(pages, languages) {
  const links = {}
  languages.map(language => links[language] = {})
  pages.map(page => {
    const teaserImage = page.metaImage?.replace('/_assets', '/assets') || '/assets/teasers/surf-mtb.jpg'
    links[page.locale][page.tags] = {
      title: page?.navigation || page.title,
      path: page.permalink,
      image: teaserImage
    }
  })
  return links
}

function countPagesPerLanguage(pages) {
  return pages.reduce((accumulator, page) => {
    if (accumulator[page.locale] == null) {
      accumulator[page.locale] = 0
    } else {
      accumulator[page.locale]++
    }
    return accumulator
  }, {})
}

async function readContents(partialsPathlist = []) {
  const promises = partialsPathlist.map(async (filePath) => {
    const fileContents = await getContent(filePath)
    return {
      path: filePath,
      ...yamlFront.loadFront(fileContents)
    }
  })
  return await Promise.all(promises)
}

function getContent(filePath) {
  try {
    return readFile(filePath, { encoding: 'utf8' })
  } catch {
    console.err('Can\'t read file', filePath)
  }
}

function getPartialsPathList(list) {
  return list.sort().filter(item => item.endsWith('.md'))
}

async function getDirList(dirPath, list = []) {
  try {
    let listing = await readdir(dirPath)
    listing = listing.sort()
    for (const item of listing) {
      // console.log(item)
      const itemPath = path.join(dirPath, item)
      const myStat = await stat(itemPath)
      if (myStat.isFile()) {
        // console.log(itemPath)
        list.push(itemPath)
      } else if (myStat.isDirectory()) {
        await getDirList(itemPath, list)
      }
    }
  } catch (err) {
    console.error(err);
  }
  return list
}


