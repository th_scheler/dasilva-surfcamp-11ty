module.exports = {
  buildTime: new Date(),
  baseUrl: "https://www.dasilva-surfcamp.de",
  iconPath: "/_assets/icons/fav",
  name: "Da Silva Surfcamp Portugal",
  twitter: "@dasilvasurfcamp",
  languages: [
    {
      label: "deutsch",
      code: "de"
    },
    {
      label: "english",
      code: "en"
    },
    {
      label: "francaise",
      code: "fr"
    },
    // {
    //   label: "espaniol",
    //   code: "es"
    // },    
  ]
};