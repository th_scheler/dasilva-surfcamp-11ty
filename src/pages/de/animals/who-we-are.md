---
tags: animals
partial: who-we-are
---

## Reiten in der Umgebung

Für unsere "großen Reiter" vereinbaren wir gerne Reitstunden oder Reitausflüge in der „Quinta do Mosteiro“, welche nur 7 Minuten mit dem Auto von uns entfernt ist.

Schaut doch mal hier: <a href="https://www.facebook.com/teamquintadomosteiro">https://www.facebook.com/teamquintadomosteiro</a><br>

### Preise pro Person:

<div style="text-align: center">
Reitstunde (30 Minuten) mit oder ohne Erfahrung = 25€<br>
Mini-Tour (30 Minuten) speziell für Kinder = 25 €<br>
Ausritt (1 Stunde, Mindestalter 10 Jahre) = 35 €<br>
Ausritt (2 Stunden) für Reiter mit Erfahrung = 70 €<br>
</div>


## Ponyreiten

Für unsere „kleinen Reiter“ vereinbaren wir gerne Ponyreiten mit Christiane’s Happy Horses in einem benachbarten Dorf.

Hier freuen sich Eliott und Timol nach wie vor über den Besuch von pferdebegeisterten Kindern und stehen an 2 bis 3 Nachmittagen pro Woche für jeweils zwei Stunden für die Gäste des Surfcamps bereit. Das zweistündige Programm mit maximal 6 Kindern beinhaltet wie immer zuerst gemeinsames Putzen, Pflegen und hübsch machen der Ponys, und anschließend wird auch gemeinsam das Satteln geübt. Danach machen wir einige Führübungen, um die Ponys vom Boden kennen zu lernen und Christiane erklärt worauf es bei der Kommunikation mit diesen tollen Tieren ankommt. Nach ein paar Tricks aus der Bodenarbeit können die Kinder sich dann abwechselnd in den Sattel schwingen und werden je nach Können von Christiane oder den anderen Kindern geführt und lernen spielerisch erst mal gut ausbalanciert und entspannt auf den Ponys zu sitzen oder reiten schon allein. Kleine Spaziergänge in die umgebende Landschaft sind auch hier möglich. Wir beenden das Programm mit dem Absatteln und Versorgen der Tiere und sollte dann noch ein bisschen Zeit sein, darf gerne noch geholfen werden ein bisschen abzuäppeln oder die Abendfütterung vorzubereiten.
Das Programm ist für Kinder von 3-12 Jahre geeignet, wobei Reiten nur bis zu einem Gewicht von maximal 50kg möglich ist. Fortgeschrittene Reitkinder können gegebenenfalls auch mit mir ausreiten. Sattelfestigkeit in allen 3 Gangarten vorausgesetzt. Kinder unter 5 Jahren kommen bitte in Begleitung eines Elternteils. (Auch wenn diese dann vielleicht nicht Händchen halten müssen)

### Preise pro Person (Kinder und Jgdl.):

<div style="text-align: center">50 € für 2 Stunden (maximal 6 Personen)<br></div>