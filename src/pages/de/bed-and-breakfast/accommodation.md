---
tags: bedAndBreakfast
partial: accommodation
---

# Unsere Unterkünfte

Das Da Silva Surfcamp Portugal verfügt über ein [Doppelzimmer](#quarto-zambujeira), ein [Drei-Bettzimmer](#quarto-abelheira), ein [Terrassenzimmer](#quarto-do-terraco) für 2-4 Personen, ein [Vier-Bettzimmer](#quarto-azul), ein [Sieben-Bettzimmer](#quarto-vermelho) und drei separate [Holzhäuschen](#tiny-house-luna) (Tiny Houses). Insgesamt haben wir damit Platz für 32 Personen. Das verteilt sich aber ganz gut, denn es gibt viele Ecken und Nischen, in die man sich zurück ziehen kann, wenn man z.B. mal in Ruhe ein gutes Buch lesen möchte.

