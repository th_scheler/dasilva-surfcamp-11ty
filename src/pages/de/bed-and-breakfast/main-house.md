---
tags: bedAndBreakfast
partial: main-house
---

## Die Zimmer im Haupthaus

Das Doppelzimmer, das 3-Bettzimmer und das Terrassenzimmer liegen im ersten Stock des Haupthauses und teilen sich ein Bad mit Dusche im Erdgeschoss, wobei das Terrassenzimmer noch ein Privat-WC hat. Der Girls-4-Bedroom liegt im Erdgeschoss des Haupthauses direkt neben dem gemütlichen Kaminzimmer. Die einzelnen Betten können nur von Frauen gebucht werden. Aber natürlich sind auch alle anderen Geschlechter erlaubt, wenn das Zimmer als Ganzes gebucht wird. Das 7-Bettzimmer befindet sich in einem Anbau neben der Küche. Die Mehrbettzimmer haben jeweils ein eigenes Duschbad. Das 7-Bettzimmer hat zusätzlich eine warme Außendusche und bietet eine kleine Kochnische mit Kühlschrank.

Die Gemeinschaftsküche, in der jeden Morgen das Frühstück serviert wird, befindet sich im Erdgeschoss direkt neben dem großen überdachten Außenbereich, wo auch die Barbecue-Abende stattfinden.
