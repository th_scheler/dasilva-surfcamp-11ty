---
tags: bedAndBreakfast
partial: quarto-azul
subtitle: Girls-4-Bedroom
---

Unser 4-Bettzimmer im Erdgeschoss erreicht man direkt vom Kaminzimmer aus. Darin stehen zwei Etagenbetten und den Gästen steht ein eigenes Duschbad zur Verfügung. Direkt neben der Tür gibt es noch ein Gäste-WC im Kaminzimmer. Die einzelnen Betten können nur von Frauen gebucht werden. Aber natürlich sind auch alle anderen Geschlechter erlaubt, wenn das Zimmer als Ganzes gebucht wird.

Preise pro Person und Nacht inkl. Frühstück und 2x BBQ pro Woche:

- Juli bis September: 35 EUR
- Oktober bis Juni: 25 EUR
- Belegung: 1-4 Personen

Hier geht es zur [Buchung]({{ links.de.onlineBooking.path }})
