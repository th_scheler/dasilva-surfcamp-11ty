---
tags: bedAndBreakfast
partial: quarto-vermelho
subtitle: Suite mit 7 Betten
---

Unsere Suite mit 7 Betten befindet sich in einem separaten Gebäude gleich um die Ecke von der Küche und schräg gegenüber von unserer Bar. In dem Raum stehen 3 Etagenbetten und über dem Badezimmer mit Dusche gibt es ein Hochbett (Galerie) mit einer weiteren Einzelmatratze ideal für Leute, die über 2 Meter groß sind, weil das Fußende offen ist. Das Zimmer ist ideal für große Familien oder größere befreundete Gruppen und bietet eine kleine Kochnische mit Kühlschrank. Draußen neben der Eingangstür kann man wunderbar auch im Freien warm duschen und dabei den Blick über die weite Landschaft genießen.

Preise pro Person inkl. Frühstück und 2x BBQ pro Woche:

- Juli bis September: 45 EUR
- Oktober bis Juni: 35 EUR
- ab 4 Personen (Erw.): 35 EUR bzw. 25 EUR
- Mindestbelegung: 3 Personen (Erw.)
- Kinder bis 12 Jahre: 50% Rabatt
- Babys bis 2 Jahre kostenlos 


Hier geht es zur [Buchung]({{ links.de.onlineBooking.path }})
