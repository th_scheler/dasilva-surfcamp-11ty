---
tags: bedAndBreakfast
partial: tiny-house-luna
---

Seit Frühjahr 2019 freuen wir uns euch unser Tiny House Luna anbieten zu können – mit allem was Mann & Frau braucht. Einer kleinen Kochnische, Kühlschrank, Bad mit WC und Dusche, und vor allem ruhig gelegen mit einer herrlichen Aussicht. Enjoy!

Die Matratze auf dem Hochbett ist 140 x 190cm groß.

Preise pro Person und Nacht inkl. Frühstück und 2x BBQ pro Woche:

- Juli bis September: 50 EUR
- Oktober bis Juni: 40 EUR
- Mindestbelegung: 2 Personen (Erw.)
- Kinder bis 12 Jahre: 50% Rabatt
- Babys bis 2 Jahre kostenlos

Hier geht es zur [Buchung]({{ links.de.onlineBooking.path }})
