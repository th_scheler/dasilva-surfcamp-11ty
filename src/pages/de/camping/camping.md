---
tags: camping
partial: camping
headline: No Glam / Just Camping!
subtitle: 
---

Für alle, die gerne sparsam unterwegs sind, haben wir die perfekte Option: das Campen! Besonders preiswert wird es, wenn Ihr euer eigenes Zelt mitbringt. Alternativ bieten wir auch Stellplätze mit Stromanschluss für Wohnmobile und Campingwagen an.

Ab 2023 steht euch unsere nagelneue Außenküche sowie ein modernes Badezimmer zur Verfügung. Wir haben diese Einrichtungen speziell für euch gebaut, damit ihr euren Aufenthalt bei uns noch angenehmer und komfortabler gestalten könnt. Genießt das Gefühl von Freiheit und Unabhängigkeit beim Campen, ohne dabei auf den Komfort einer Küche und eines modernen Badezimmers verzichten zu müssen und seid mitten im Surfcamp-Leben.


* Übernachtung im eigenen Zelt, Wohnmobil oder Campingwagen: 10 EUR pro Person und Nacht (Kinder bis 2 sind kostenfrei und Kinder bis 12 zahlen die Hälfte)
* Stellplatzgebühr für Wohnmobil oder Campingwagen: 10 EUR pro Nacht inkl. Strom
* Alle Preise inklusive Mitbenutzung von Badezimmer und Außenküche
* NICHT enthalten im Preis sind das tägliche Frühstücksbuffet (7 EUR) und unser BBQ (13 EUR, Sonntag und Freitag). Dies könnt ihr aber gerne als Zusatzleistung dazubuchen und euer Camping-Erlebnis noch unvergesslicher machen.


Die nächste Entsorgungsstation ist nur einen Katzensprung von uns entfernt. Sie befindet sich beim Intermarché-Supermarkt in Lourinhã und ist in nur 5 Minuten erreichbar. Außerdem bieten wir euch die Möglichkeit euren Frischwassertank bei uns aufzufüllen.
