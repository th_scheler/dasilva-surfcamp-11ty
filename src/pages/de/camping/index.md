---
permalink: /de/camping/index.html
layout: camping.njk
tags: camping
locale: de
navigation: Camping

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Surf und Aktivurlaub für die Familie"

# max 158 characters
metaDescription: "Ein typisch portugiesisches Landhaus mitten in der Natur, nahe zu traumhaften Surfstränden und ideal für Familien mit Kindern"
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/camping.jpg"
---
