---
permalink: /de/kontakt/index.html
layout: contact.njk
tags: contact
locale: de
navigation: Kontakt

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Kontakt"

# max 158 characters
metaDescription: "Kontaktformular und Adresse"
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/surf-mtb.jpg"
---
