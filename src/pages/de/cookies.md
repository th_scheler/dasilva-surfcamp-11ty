---
permalink: /de/cookies/index.html
layout: plain-text.njk
tags: cookies
locale: de
navigation: Cookies

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Cookies"

# max 158 characters
metaDescription: "Cookies | Privacy Policies"
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/surf-mtb.jpg"
---

# Cookies

Wir nutzen 2 Arten von Cookies: Marketing Cookies und funktionale Cookies.

Wie du die Cookies zulassen oder ablehnen kannst, erfährst du hier auf der Seite.

## Marketing Cookies

Die Marketing Cookies helfen uns zu verstehen, wie ihr uns gefunden habt. Damit können wir unsere Marketing-Aktionen besser abstimmen. Die gesetzten Cookies werden von Google Analytics ausgewertet. Details zum Thema Datenschutz findest du auf unserer [{{ links[locale].privacyPolicies.title }}]({{ links[locale].privacyPolicies.path }}).

Wenn du keine Marketing-Cookies möchtest, kannst du unten auf der Seite auf dem **Cookie Consent Banner** auf "Marketing-Cookie ablehnen" klicken. Du tust uns einen Gefallen, wenn du auf "Cookies akzeptieren" klickst :-)

## Funktionale Cookies

Funktionale Cookies sind Cookies, die aus technischen Gründen notwendig sind. Auf unserer Site haben wir 2 Cookies: Eins für Videos und eins fürs Online Booking.

### Video Cookie

Das Cookie für Videos wird genau dann gesetzt, wenn du auf ein Video klickst. Wir nutzen den Videoplayer von "Vimeo", der das Cookie braucht, damit klar ist, wann der Videoplayer geladen ist und er bereit ist das Video abzuspielen. Mehr Details findest du hier: <a href="https://vimeo.com/cookie_policy" target="_blank">Vimeo Cookie Policy</a>

Wenn du kein Video Cookie haben möchstest, dann klicke auf kein Video. 

### Online Booking Cookie

Wenn du auf Online Booking gehst, wird ein Cookie genutzt. Wir nutzen den Dienstleister "Bookinglayer" für das Handling der Buchung und Zahlung. Dieser Dienstleister ist nur mit Cookie nutzbar. Mehr Details findest du hier: <a href="https://www.bookinglayer.com/cookie-policy" target="_blank">Booking Layer Cookie-Policy</a>.

Wenn du dieses Booking Cookie nicht haben möchtest, dann kannst du unsere [{{ links[locale].bookingRequest.title }}]({{ links[locale].bookingRequest.path }}) nutzen oder uns direkt [{{ links[locale].contact.title }}]({{ links[locale].contact.path }}).

