---
tags: home
partial: prices
---

Ganz gleich ob Ihr auf eigene Faust die Spots und Wellen der Gegend erkunden oder Euch lieber einem Surfkurs anschließen möchtet, das Da Silva Surfcamp Portugal bietet Euch eine sehr freundliche und entspannte Atmosphäre. Los geht es [ab 25 €]({{links.de.prices.path}}) die Nacht oder im Surfpackage ab 415 € die Woche.
