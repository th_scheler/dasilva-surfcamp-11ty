---
permalink: /de/urlaubsinfo/index.html
layout: plain-text.njk
tags: info
locale: de
navigation: Urlaubsinfo

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Urlaubsinfo"

# max 158 characters
metaDescription: "Wichtige wohlfühl-Infos zur Verpflegung, Mücken, Wäschewaschen uvm."
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/surf-mtb.jpg"
---

# Urlaubsinfo

<div class="h2">Herzlich Willkommen im
<br/>Da Silva Surfcamp</div>
<div class="h2">Olá e bem vindo!</div>

Wir freuen uns Euch als Gäste bei uns begrüßen zu dürfen. Vor Euch liegen ein paar entspannte Tage voll Sonne und grandioser Wellen. 

Das Camp-Team ist stets für Euch vor Ort – ob Hilfe, Fragen, Anregungen oder Sonstiges – wir sind für Euch da, um Euren Urlaub hier so angenehm wie nur möglich zu gestalten. 

Damit Eurem Surfurlaub nichts mehr im Wege steht, bitten wir Euch freundlich um Eure Mithilfe. Auf den folgenden Seiten findet Ihr eine kleine Zusammenfassung wissenswerter Eckpunkte, um deren Einhaltung und Beachtung wir Euch bitten.

**Wir wünschen euch eine schöne Zeit bei uns**

### Zufriedenheit

Unser größter Wunsch ist, dass ihr mit allem bei uns zufrieden seid und euren Aufenthalt in schönster Erinnerung behaltet. Sprecht uns einfach an, wenn ihr mit dem ein oder anderen nicht so zufrieden seid, wir werden uns bemühen, das schnellstmöglich zu verbessern.

### Frühstück

Täglich erwartet euch ein frisches und reichhaltiges Frühstücksbuffet in der Küche des Haupthauses. Die Frühstückszeiten sind abhängig vom Pick-Up der Surfschule. Allgemein gilt, dass das Buffet eine Stunde vor Abfahrt für euch bereit steht und ihr bis 10 Uhr die Möglichkeit habt zu frühstücken. Am Samstag und Sonntag finden in der Regel keine Surfkurse statt. An diesen Tagen sind die Frühstückszeiten von 09:00 - 10:00 Uhr. 

Um unsere Gemeinschaftsküche im Camp für alle Gäste in Ordnung zu halten, bitte wir Euch, euer benutztes Geschirr selbst in die Spülmaschine zu räumen bzw. abzuspülen falls diese bereits voll ist. 

Außerhalb der Frühstückszeiten bitten wir euch zu dem die benutzten Töpfe, Pfannen, Schüsseln, etc. nach dem Essen abzuspülen, abzutrocknen und in die Schränke zu verräumen.

### Kühlschränke

Während eures Aufenthaltes gibt es täglich ein leckeres Frühstück und an zwei Abenden der Woche (immer Sonntag und Freitag) veranstalten wir gemeinsam ein ausgiebiges BBQ. Wir bitten euch, das Frühstücksbuffet nicht zu nutzen, um euch für den weiteren Tag ein Lunch Paket zu schmieren, so kalkulieren wir nicht und jede*r soll vom Frühstück satt werden ;)

Für eure weitere Verpflegung stehen euch die Gästekühlschränke im Haupthaus zur Verfügung sowie die Fächer im großen Regal. Diese sind mit den Zimmernamen versehen. Seid ihr ohne Auto vor Ort – kein Problem, wir fahren euch gern zum Supermarkt nach Lourinhã, sprecht uns einfach an. Weiterhin findet ihr in Praia da Areia Branca einen kleinen Supermarkt, einen Obstladen und rechts daneben sogar einen kleinen Markt. Solltet ihr mal Essen gehen oder bestellen wollen, sprecht uns einfach an – neben Empfehlungen können wir auch gerne Fahrten mit euch unternehmen.

### Fliegen und Mücken

Leider gibt es bei uns sehr viele Fliegen. Es ist daher absolut wichtig, dass alle Türen und Fenster, die nicht mit einem Fliegengitter versehen sind, immer geschlossen bleiben. Gleiches Gilt für die Mücken. Vor allem in der Abenddämmerung sollten alle Fenster und Türen geschlossen sein. Wir haben einen großen Vorrat an Fliegenfängern. Sprecht uns gerne an, falls ihr Nachschub benötigt.

### Ruhezeiten

Surfen schlaucht – manche von euch wissen das bereits, die anderen werden es schnell merken. Gemütliche Runden sollen nicht unterbrochen werden, aber nehmt bitte Rücksicht auf die anderen Gäste und haltet die Nachtruhe ab 23 Uhr ein.

### Handtücher

Jeder Gast bekommt bei seiner Anreise ein großes und ein kleines Handtuch. Solltet ihr zwischendurch mal ein frisches Handtuch brauchen, sprecht uns bitte einfach an. Bitte nehmt unsere Handtücher nicht mit an den Strand und nutzt hierfür eure eigenen Strandtücher. 

### Bettwäsche

Die Betten sind bei der Anreise frisch bezogen. Wenn Ihr länger als eine Woche bei uns seid, könnt Ihr gern frische Bettwäsche bekommen. Bitte sprecht uns einfach an.

Zu jedem Zimmer gibt es einen Schlüssel, der im Schloss steckt, wenn ihr anreist. Bei den Tiny Houses gitb es vorne eine Schlüsselbox. Wenn möglich lasst eure Zimmer donnerstags und sonntags auf, damit wir frische Handtücher hinlegen und einmal durchsaugen können.

### Schlüssel

Zu jedem Zimmer gibt es einen Schlüssel , der bereits im Schloss steckt, wenn ihr anreist. Bei den Tiny Häusern könnt ihr gerne auch die Schlüsselboxen nutzen. Wichtig: Bitte nicht den Code für die Schlüsselbox ändern!

### Wäsche waschen

Wenn Ihr eure Wäsche waschen wollt, gibt es in Praia da Areia Branca direkt neben dem „SPAR“ Supermarkt eine Self-Service-Wäscherei. Kleingeld für die Maschinen bekommt ihr direkt vor Ort. Ihr könnt auch die Waschmaschine in unserer Waschküche nutzen. Wir berechnen dafür 5 EUR pro Maschine inklusive Waschmittel. Bitte tragt die 5 EUR einfach auf der Barliste ein.

### Wäsche trocknen

Auf dem Gelände findet ihr mehrere Spots, wo ihr eure Wäsche, Strandtücher und Wetsuits zum Trocknen aufhängen könnt. Zum einen findet ihr Wäscheleinen hinter dem Spielplatz und zum anderen an der Hauswand neben der Küche.

### Ordnung und Sauberkeit

Bitte achtet generell darauf, immer alles so zu hinterlassen, wie Ihr es vorgefunden habt. Wenn Ihr abends zusammen in der Küche kocht, bitten wir Euch darum, sämtliches Geschirr, Besteck, Töpfe und Pfannen selber abzuwaschen, bzw. die Spülmaschine zu benutzen. Sauberes Geschirr, Töpfe und Pfannen bitte abtrocknen und in die Küchenschränke einräumen, so dass der nächste Gast Platz für seinen Abwasch hat.
Gleiches gilt für leere Flaschen und Gläser - diese gerne noch am selben Abend wegräumen - das Surfcamp-Team wird es euch danken ;-)

### Rauchen

Lediglich im Außenbereich ist das Rauchen erlaubt. Achtet bitte darauf keine Zigarettenstummel auf den Boden fallen zu lassen. Zum einen wegen der Umweltauswirkungen und zum anderen muss keine zweite Person sie wieder aufheben. Außerdem reicht schon eine unachtsam weggeworfene Zigarette in den trockenen Sommermonaten aus, um ein Feuer zu entfachen.

### Freizeitangebote

...gibt es nahezu unbegrenzt. Kart fahren, Reiten, Tennis, Wandern, Kiten, Dinopark und vieles Mehr. Wir kennen uns hier in der Gegend sehr gut aus und können euch viele Tipps geben. Wir wollen auf keinen Fall, dass Ihr Euch bei uns langweilt, sprecht uns daher bitte einfach an oder werft einen Blick auf unsere Tafel mit den täglichen Aktivitäten sowie in unsere Ausflugmappe, die im Wohnzimmer ausliegt.

### Abreise mit dem Expressbus

Spätestens einen Tag vor Abreise solltet ihr online ein Busticket kaufen. Es kann durchaus vorkommen, dass der Expressbus bereits ausgebucht ist. Bitte unbedingt darauf achten, dass das Busticket von Praia da Arara Branca nach Lissabon Sete Rios ausgestellt ist. Solltet ihr dabei Unterstützung benötigen, sprecht uns einfach an und wir helfen euch gerne bei der Buchung.

### Beachcruiser

Gerne könnt ihr euch einen unserer Beachcruiser ausleihen. Damit seid Ihr völlig unabhängig und könnt jederzeit zum Strand, zum Einkaufen nach Lourinhã oder abends nach Areia Branca zum Ausgehen fahren. Bitte denkt daran immer ein Schloss mitzunehmen und das Fahrrad an einem festen Gegenstand anzuschließen. Wenn ihr nach Sonnenuntergang unterwegs seid und Licht benötigt, sagt uns einfach Bescheid. Wenn ihr das Fahrrad nicht mehr benötigt, stellt es bitte zurück in den Fahrradschuppen und lasst die Fahrräder bitte nicht an den Surflodges stehen, damit die Fahrräder jedem Gast zur Verfügung stehen.

Viele Infos, solltet ihr dennoch was auf dem Herzen haben sind wir jederzeit für eure Fragen da, sprecht uns einfach oder meldet euch im voraus via Whatsapp oder Signal: +351 / 261 461 515

**Hang Loose**

**Euer Team vom Da Silva - Surfcamp**