---
permalink: /de/kids-are-welcome/index.html
layout: kids-are-welcome.njk
tags: kidsAreWelcome
locale: de
navigation: Kids Welcome

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Kinder sind willkommen"

# max 158 characters
metaDescription: "Spass für die ganze Familie | Am Strand mit Mama und Papa surfen oder im Meer | Pferde reiten im Camp | viele andere Kinder | famliy"
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/kids-are-welcome.jpg"
---
