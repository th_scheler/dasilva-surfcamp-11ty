---
tags: mountainBike
partial: mtb-package-info
---

<div class="h3">Noch ein paar kurze Infos:</div>

Wer mit Klickies fahren möchte, sollte seine eigenen Pedalen und Schuhe mitbringen. Außerdem empfehlen wir einen eigenen Helm dabei zu haben, Bike-Handschuhe und einen Rucksack für Proviant (Wasser, Obst und ein paar Müsliriegel). Am besten wäre, ein Camel-Back (Trinkwasser-Rucksack). Eine Radsporthose (gepolstert) und im allgemeinen Funktions-Sportwäsche (mind. 2x komplett) sollte man auch mitnehmen. Unsere Guides haben bei den Touren immer Flickzeug, Reifenheber, Pumpe, Ersatzschläuche, ein Schaltauge und ein Erste-Hilfe-Set dabei.

Wir unternehmen keine gemütlichen Spazierfahrten durch die Landschaft, sondern sportlich anspruchsvolle MTB-Touren, die nichts für Anfänger sind. Allgemeine körperliche Fitness und Erfahrung beim Radfahren sind Voraussetzung für die Teilnahme.
