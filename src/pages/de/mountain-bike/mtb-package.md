---
tags: mountainBike
partial: mtb-package
---

## MTB-Package

### ab 475 € pro Woche / Person

<div class="h4">inklusive</div>

* 7 Tage Bed & Breakfast in unserem Camp
* 2x Barbecue (auch vegetarisch) mit Lagerfeuer
* 5 Mountainbike Touren (4-6 Stunden / 40-60 km pro Tag)
* High Level CUBE Fully Mountainbike (29 Zoll) mit Helm
* Shuttle-Bus zu den besten MTB-Trails in unserer Gegend
