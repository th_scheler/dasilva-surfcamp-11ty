---
permalink: /de/mtb-tours/barragem-sao-domingos/index.html
layout: mtb-tours-barragem-sao-domingos.njk
tags: mtbTourBarragemSaoDomingos
locale: de
navigation: Barragem Sao Domingos

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Mountain Bike Tours » Barragem Sao Domingos"

# max 158 characters
metaDescription: "Die MTB Tour führt durch Barragem de São Domingos an der Küste entlang nach Consoloção bis zum Stausee von Atouguia da Baleia Portugal"

# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/barragem-sao-domingos.jpg"
---
