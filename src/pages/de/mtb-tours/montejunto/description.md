---
tags: mtbTourMontejunto
partial: description
---

Nach einem Tag Pause im Camp erwartet uns heute die große Bergwertung am Montejunto. Mit dem Camp-Bus geht es nach Vila Verde dos Francos, von wo aus wir den etwa 10 Kilometer langen Anstieg zum Gipfel beginnen.

Auf der Nordseite des Montejunto ist es am Morgen noch etwas kühler und so fährt sich die Strecke, zudem auf Asphalt, ziemlich glatt in einem durch. Auf dem Gipfel angekommen haben wir an einer alten Klosterruine auf ca. 660 Metern über NN einen tollen Rundumblick und ein frischer Wind verschafft Abkühlung.

Vom Gipfel aus machen wir einen Schlenker zur Bergspitze am Penha do Meio Dia und fahren von dort über das umliegende Plateau Richtung Osten. Die Trails hier sind steinig und der Bewuchs eher karg. Erst mit dem Abstieg erreichen wir wieder mehr Wald. Daniel führt uns querfeldein über schnell wechselnde Trails und Wege. Mal unter Bäumen auf weichem Waldboden, dann wieder auf harten, ausgewaschenem Waldwege mit tiefen Rillen. Der Weg bergab ist lang und ausgiebig und wir erreichen schließlich das Tal auf der Südseite des Berges. Die satte Landschaft ist vom Weinbau geprägt und unterscheidet sich wiederum komplett von allen vorherigen Touren. Bei Kilometer 25 legen wir eine Rast ein. Auf der Südseite des Montejunto erwarten uns jetzt drei steile Anstiege. Von etwa 130 Metern über NN klettern wir nun noch einmal auf 420 Meter über NN. Es ist heißer als beim ersten Aufstieg und nach den technischen Passagen am Berg sind die Kräfte nicht mehr ganz so da wie noch am Morgen. Je nach Bedarf sollte man für diese 
Tour bei sommerlichen Temperaturen mindestens 3 Liter Wasser dabei haben und einige Zwischenmahlzeiten.

Der Aufstieg belohnt mit sehr schönen Aussichten auf die Weinberge südlich des Montejunto, zehrt aber wirklich an den Kräften. Vom Höhepunkt dieser Strecke aus steigen wir steil ab, bis wir die Weinberge erreichen. Mit den letzten Reserven steigen wir noch einmal 115 Höhenmeter auf um danach die Abfahrt auf den Trails zu genießen.

Auf dem Weg zurück nach Vila Verde dos Francos drehen wir ein paar Ehrenrunden um eine historische Windmühle, bevor wir uns, im Dorf angekommen, mit eiskaltem Superbock 0.0 und einer Tüte Chips belohnen.

Montejunto war, allein von der Kondition her, die härteste Tour der Woche. Wer gute Fitness mitbringt und auf die Verpflegung unterwegs achtet (2,5 Liter Wasser, ein Apfel, eine Banane, Cracker und zwei Käsebrote waren nur grade so ausreichend), sollte auch die Bergwertung gut packen.
