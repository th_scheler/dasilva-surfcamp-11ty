---
permalink: /de/mtb-tours/montejunto/index.html
layout: mtb-tours-montejunto.njk
tags: mtbTourMontejunto
locale: de
navigation: Montejunto

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Mountain Bike Tours » Montejunto"

# max 158 characters
metaDescription: "Einsame MTB Singletrails führen durch eine unberührte raue Berg-Landschaft des Serra de Montejunto in Portugal"

# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/montejunto.jpg"
---
