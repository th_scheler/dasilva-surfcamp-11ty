---
tags: prices
partial: bedAndBreakfast
---

# Und was kostet der Spaß?

## Bed & Breakfast

### ab 25 € pro Tag / Person

<div class="h4">inklusive:</div>

* Unterkunft und tägliches Frühstücksbuffet
* 2x die Woche Barbecue (auch vegetarisch) ab 1 Woche Aufenthalt
* Bettwäsche und Handtücher
* täglicher Shuttlebus in den nächsten Strandort
* kostenloser Verleih von Beach-Cruiser
* freies W-Lan im Haupthaus
