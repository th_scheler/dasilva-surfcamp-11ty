---
tags: prices
partial: directions-tips-activities
---

## An- und Abreisetag

Theoretisch kann man bei uns an jedem beliebigen Wochentag an- und abreisen. Da die Surfkurse aber immer montags anfangen und wir am Sonntag Abend unser berühmt-berüchtigtes Welcome-BBQ machen, empfiehlt es sich am Sonntag an- und abzureisen, nicht zuletzt auch wegen der Gruppendynamik, die sich im Laufe einer Woche automatisch entwickelt.

Apropos, wie Ihr zu uns findet, erfahrt Ihr hier: [Anreise]({{links.de.travelDirections.path}}).

## Beachcruiser

Für den Weg zum Strand oder einfach nur so zum Rumfahren könnt Ihr Euch bei uns jederzeit kostenlos schicke Beachcruiser ausleihen. Das ist cool und macht die Beine locker :o)

## Verpflegung

Das tägliche Frühstücksbuffet findet ihr in der Surfcampküche und könnt ihr im Außenbereich oder dem Wohnzimmer genießen. Die zweimal wöchentlich stattfinden BBQs findet immer sonntags und freitags im Außenbereich statt. Bei der Zubereitung des Frühstücks und der BBQs achten wir immer auf reichlich vegetarische und vegane Auswahl.

## Weitere Verpflegungsmöglichkeiten

Im Strandort Praia da Areia Branca (2 km) gibt es einen kleinen Supermarkt und einen täglichen Wochenmarkt mit frischem Obst und Gemüse, Fisch etc.. Da bekommt man eigentlich alles, was man braucht. In der Kreisstadt Lourinha (4 km) gibt es größere Supermärkte (z.B. auch LIDL und ALDI), wo es eine noch größere Auswahl gibt und die jeden Tag bis 21 Uhr geöffnet haben. Wenn Ihr kein eigenes Auto habt, nehmen euch unsere Teamer auch zum Einkauf mit. Davon abgesehen gibt es überall viele günstige Cafés und Restaurants, wo man leckeres typisch portugiesisches Essen bekommt. Es gibt aber auch eine Pizzeria, einen besonders guten Inder direkt am Strand und einen Fast-Food-Imbiss an der Landstraße. Ansonsten gibt es auch verschiedene Möglichkeiten Essen direkt zu uns ins Surfcamp zu bestellen. Unsere Teamern machen auch gern eine Sammelbestellung für alle Gäste und holen das Essen dann für Euch ab.

