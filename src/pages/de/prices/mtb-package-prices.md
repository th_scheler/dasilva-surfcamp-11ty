---
tags: prices
partial: mtb-package-prices
---

- Low Season
  - (October – June)
  - Multiple: 475 €
  - Double: 510 €
  - Suite: 545 €
  - Tiny House: 580 €
  - Single: 685 €
- High Season
  - (July – September)
  - Multiple: 545 €
  - Double: 580 €
  - Suite: 615 €
  - Tiny House: 650 €
  - Single: 755 €
