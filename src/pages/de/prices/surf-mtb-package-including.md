---
tags: prices
partial: surf-mtb-package-including
---

<div class="h4">inklusive</div>

* 7 Tage Bed & Breakfast im Da Silva Camp
* 2x Barbecue (auch vegetarisch) mit Lagerfeuer
* 5 Tage Action mit Surfboard oder Mountainbike
* 2 oder 3 Tage Surfunterricht (2 Stunden pro Session / 2 Sessions pro Tag) komplettes Surf Equipment (Surfboard, Leash, Wetsuit)
* Shuttle-Bus zu den Stränden mit den besten Wellen in unserer Gegend
* 2 oder 3 Mountainbike Touren (4-6 Stunden / 40-60 km pro Tag)
* High Level CUBE Fully Mountainbike (29 Zoll) mit Helm
* Shuttle-Bus zu den besten MTB-Trails in unserer Gegend
  
