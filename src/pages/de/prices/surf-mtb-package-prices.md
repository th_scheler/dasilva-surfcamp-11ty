---
tags: prices
partial: surf-mtb-package-prices
---

- Low Season
  - (October – June)
  - Multiple: 475 €
  - Double: 510 €
  - Suite: 545 €
  - Tiny House: 580 €
  - Single: 685 €
- High Season
  - (July – September)
  - Multiple: 560 €
  - Double: 595 €
  - Suite: 630 €
  - Tiny House: 665 €
  - Single: 770 €
