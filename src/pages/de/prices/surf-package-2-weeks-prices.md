---
tags: prices
partial: surfPackage2WeeksPrices
---

- Low Season
  - (October – June)
  - Multiple: 810 €
  - Double: 880 €
  - Suite: 960 €
  - Tiny House: 1.020 €
  - Single: 1.230 €
- High Season
  - (July – September)
  - Multiple: 980 €
  - Double: 1.050 €
  - Suite: 1.120 €
  - Tiny House: 1.200 €
  - Single: 1.400 €
