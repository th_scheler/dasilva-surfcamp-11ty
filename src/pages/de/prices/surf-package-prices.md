---
tags: prices
partial: surfPackagePrices
---

- Low Season
  - (October – June)
  - Multiple: 415 €
  - Double: 450 €
  - Suite: 485 €
  - Tiny House: 520 €
  - Single: 625 €
- High Season
  - (July – September)
  - Multiple: 505 €
  - Double: 540 €
  - Suite: 575 €
  - Tiny House: 610 €
  - Single: 715 €
