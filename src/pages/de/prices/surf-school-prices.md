---
tags: prices
partial: surfSchoolPrices
---

- Low Season
  - (Oct. – June)
  - 1 day /<br/>2 sessions: 60 €
  - 5 days /<br/>10 sessions: 240 €
  - 10 days /<br/>20 sessions: 460 €
- High Season
  - (July – September)
  - 1 day /<br/>2 sessions: 65 €
  - 5 days /<br/>10 sessions: 260 €
  - 10 days /<br/>20 sessions: 490 €
