---
tags: prices
partial: surfYogaPackagePrices
---

- Low Season
  - (October – June)
  - Multiple: 515 €
  - Double: 550 €
  - Suite: 585 €
  - Tiny House: 620 €
  - Single: 725 €
- High Season
  - (July – September)
  - Multiple: 605 €
  - Double: 640 €
  - Suite: 675 €
  - Tiny House: 710 €
  - Single: 815 €
