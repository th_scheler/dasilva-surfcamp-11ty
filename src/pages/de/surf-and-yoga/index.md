---
permalink: /de/yoga/index.html
layout: yoga.njk
tags: yoga
locale: de
navigation: Yoga

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Surf und Yoga"

# max 158 characters
metaDescription: "Yoga für körperliches, emotionales und geistiges Wohlbefinden | traditionelle Hatha Yoga Praxis | Surf und Yoga Package"
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/yoga.jpg"
---
