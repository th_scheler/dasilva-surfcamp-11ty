---
permalink: /de/surf-lodges/index.html
layout: surf-lodges.njk
tags: surfLodges
locale: de
navigation: Surf Lodges

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Surf Lodges"

# max 158 characters
metaDescription: "Surf Lodges für die anspruchsvolleren Surfer und Mountain Biker, nahe traumhafter Surfstränden und ideal für Päarchen, Gruppen oder Familien mit Kindern"
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/surf-lodges.jpg"
---
