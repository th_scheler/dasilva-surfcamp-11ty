---
tags: surfLodges
partial: studio-prata
---
Die Gäste des Studio Prata teilen sich den Innenhof mit Casa Francisco. Die kleine aber sehr komfortable Ferienwohnung bietet Platz für zwei Personen. Ein Doppelbett, Küchenzeile, Esstisch, Smart-TV und ein modernes Duschbad sollten keine Wünsche offen lassen. Es stehen Gartenmöbel und eine Grillstelle zur Verfügung. Auf dem Dach gibt es eine große Terrasse mit schönem Blick über das Land. Die Besitzer wohnen im Haupthaus und sind sehr freundlich und hilfsbereit. Auch von hier sind es nur 400m zu unserem Surfcamp. Ein Feldweg führt zu wunderschönen Badebuchten, die selbst in der Hochsaison menschenleer sind.

<b>Preise pro Person inkl. Frühstück* und 2x BBQ pro Woche:</b>

- für 2 Personen: 40 EUR
- inkl. 2x BBQ pro Woche im Surfcamp
- inkl. 1x gefüllter Kühlschrank oder Frühstücksbuffett im Surfcamp
- Kinder bis 12 Jahre: 50% Rabatt
- Babys bis 2 Jahre: kostenlos

Hier geht es zur [Buchung]({{ links.de.onlineBooking.path }})
