---
tags: surfLodges
partial: welcome
---

# Da Silva Surf Lodges

## Holiday Homes, Apartments & Studios

### für den individuellen Surfurlaub in Portugal
<br>
In der näheren Umgebung unseres Surfcamps gibt es verschiedene Ferienhäuser, Apartments und Studios, die sich ideal für einen Surfurlaub eignen. Die ruhige Lage, ein komfortables Wohnzimmer mit Terrasse und die gut ausgestattete Küche, in der man sich selbst verpflegen kann, ermöglichen hier einen individuellen Surfurlaub. Dabei muss man nicht auf die Annehmlichkeiten eines Surfcamps verzichten.

Alle unsere Sportangebote, wie Surfkurse, Mountainbiken, Pony reiten, Yoga, Trailrunning können dazu gebucht werden. Davon abgesehen kann man jederzeit bei uns vorbei kommen und sich dazu gesellen. Unser Barstuff freut sich über jeden Besuch und die Kids können in den Pool springen, während sich die Eltern an der Bar erfrischen. Nicht selten enden die dort begonnenen Gespräche an einem romantischen Lagerfeuer. Im Preis enthalten sind zwei BBQs pro Woche und ein gefüllter Kühlschrank für das tägliche Frühstück oder nach Wunsch das Frühstücksbuffett im Surfcamp.
