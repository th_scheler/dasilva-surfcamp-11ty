---
tags: surroundings
partial: baleal
mapLink: https://goo.gl/maps/n6XtzJkkn3iUbPgF8
---

Das Surfmekka von Europa (12 km nördlich) mit über 40 Surfcamps und nettem Nachtleben. Etwas überlaufen, aber immer einen Besuch wert.
