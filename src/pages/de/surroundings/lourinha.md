---
tags: surroundings
partial: lourinha
mapLink: https://goo.gl/maps/pp7DREaLrA5FBZfHA
---

Die Kreisstadt (5 km), wo man Banken, Apotheken, Ärzte, Post, große Supermärkte, Boutiquen, einen Wochenmarkt und typische Cafés und gute Restaurants findet.
