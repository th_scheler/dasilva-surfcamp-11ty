---
tags: surroundings
partial: praia-areia-branca
mapLink: https://goo.gl/maps/SvoMUacFBzMmB7bj7
---

Unser „Homespot“ (2 km, mit dem Fahrrad 10 min.) mit Strandpromenade, guten Wellen und super Nachtleben. Hier hält auch der Expressbus aus Lissabon.
