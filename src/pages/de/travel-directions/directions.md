---
tags: travelDirections
partial: directions
---

## Allgemeine Infos

Der näheste Flughafen ist in Lissabon, ca. 65 km südlich von uns. Von dort kommt man sehr bequem und preiswert mit dem Expressbus direkt nach Praia da Areia Branca, wo wir Euch gern von der Bushaltestelle abholen. Ansonsten kann man sich auch am Flughafen einen Mietwagen nehmen, mit dem Taxi anreisen oder unseren Flughafen-Transfer nutzen. Die Anreise im Surfcamp ist Sonntags ab 16 Uhr und die Abreise bis 11 Uhr möglich. Vorher und nachher könnt Ihr natürlich gern Euer Gepäck bei uns unterstellen und die Zeit bei uns geniessen.

## Expressbus

Der Expressbus startet am Busbahnof "Sete Rios". Um zum Busbahnhof zu kommen, könnt ihr entweder mit der U-Bahn, Uber oder Taxi fahren.

**U-Bahn:** Nach eurer Ankunft verlasst ihr den Flughafen nach rechts in Richtung Metro. Am Ticketautomaten kauft ihr euch je Person ein wiederaufladbares Ticket (50 Cent) mit einer Fahrt (1,10 EUR). Dann nehmt ihr die rote Linie "Vermelho" bis nach "Sao Sebastiao", steigt um in die blaue Linie "Azul" in Richtung "Reboleira" und steigt an der Haltestelle "Jardim Zoologico" aus.

**Uber:** Nach eurer Ankunft (EG) müsst ihr zum "Passenger Drop Off" vor der Departure Halle (OG) gehen, da Uber nur hier halten darf. Hier könnt ihr ein Uber zum Busbahnhof nehmen oder euch auch direkt ins Surfcamp fahren lassen.

**Taxi:** Hier muss man aufpassen, dass man nicht beschummelt wird. Die Fahrt sollte nicht mehr als 10 EUR kosten. Ein Tipp: Nicht die Taxis nehmen, die bei den Arrivals warten, sondern zu den Departures gehen und dort ein Taxi nehmen, das gerade zufällig jemanden zum Flughafen gebracht hat.

Angekommen am Busbahnhof „Sete Rios“ muss man sich am Schalter ein Ticket nach Praia da Areia Branca kaufen, bevor man in den Expressbus mit Endstation "Peniche" steigt. Unter diesem Link kann man die Tickets auch online kaufen oder die sehr praktische App downloaden, mit der man die Bustickets auf seinem Smartphone abspeichern kann: <a href="https://www.rede-expressos.pt/" target="_blank">rede-expressos.pt</a>

Die Fahrt mit dem Expressbus von Lisboa nach Praia da Areia Branca kostet 8,90 EUR und dauert genau 1 Stunde und 15 Minuten. Die Endstation ist Peniche. Praia da Areia Branca ist die vorletzte Haltestelle und kein Busbahnhof, sondern nur eine kleine unauffällige Bushaltestelle mitten im Ort Praia da Areia Branca. Wenn man durch die Kreisstadt Lourinha gefahren ist, muss man an der nächsten Haltestelle aussteigen.

Am Besten Ihr versucht den Bus zu nehmen, der um 17:00 oder 18:15 Uhr in Praia da Areia Branca ankommt. Dann seid Ihr genau pünktlich zum Welcome-BBQ, das jeden Sonntag bei uns stattfindet.

## Flughafen-Transfer

Wenn ihr persönlichen Service möchtet, holen wir euch auch gerne und jederzeit vom Flughafen in Lissabon ab oder bringen Euch dorthin. Das ist vor allem sinnvoll, wenn Euer Flug ausserhalb der Fahrzeiten vom Expressbus stattfindet. Wir empfangen euch im Ankunftsbereich bei Starbucks. Ihr erkennt uns am Da Silva T-Shirt. Gerne halten wir auf der Fahrt ins Surfcamp kurz  (30 Minuten) an einem Supermarkt, so dass ihr euch mit dem Wichtigsten für die ersten Tage versorgen könnt. In unseren Shuttlebus passen bis zu 8 Personen. Bitte unbedingt den Flughafen-Transfer rechtzeitig bei uns anmelden und bestätigen lassen.

```
Preis für 1 - 4 Personen = 80 EUR
Preis für 5 - 8 Personen = 120 EUR
+ 20 EUR Nachtzuschlag (von 01:00 bis 05:00 Uhr)
```

## Anreise mit dem Auto

Falls Ihr mit dem Auto kommt, mit dem **Navigationsgerät: NICHT zur postalischen Adresse** fahren! Es gibt in unserer Gegend **mehrere Strassen mit dem gleichen Namen** und man wird daher oft zur falschen Adresse navigiert. Die sicherste Art und Weise uns zu finden ist bei **Google-Maps: „Da Silva Surfcamp“** einzugeben.

## Kurzer Stopover in Lissabon?

Ihr wollt vor oder nach eurem Aufenthalt bei uns das schöne Lissabon erkunden? Dann können wir euch das Hostel <a href="https://www.lisboncalling.net/" target="_blank">Lisbon Calling</a> absolut empfehlen. Es ist super schön gelegen, nur 3 Minuten zu Fuss zum angesagtesten Szeneviertel "Cais Sodré" und nur 10 Minuten zu Fuss zum „Bairro Alto", wo es eine coole Kneipe neben der anderen gibt. Ausserdem sind die Leute die dort arbeiten extrem nett und das Hostel ist total schön eingerichtet. Am besten mal die Homepage angucken und sich ein eigenes Bild machen :-)

Und wer es etwas ruhiger und mit eigenem Badezimmer haben möchte, ist in der neuen Unterkunft vom <a href="https://www.lisboncalling.net/double-rooms-studio/" target="_blank">Lisbon Calling in der Rua da Fé</a> besser aufgehoben.