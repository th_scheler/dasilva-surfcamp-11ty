---
tags: animals
partial: offers
---
## Horse rides in the area

For our "big riders" we are happy to arrange riding lessons or horseback riding excursions in the "Quinta do Mosteiro", which is only 7 minutes away by car.

Have a look here:
https://www.facebook.com/teamquintadomosteiro/


