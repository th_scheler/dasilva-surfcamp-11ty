---
tags: bedAndBreakfast
partial: accommodation
---

## Our accommodations

The Da Silva Surf Camp Portugal has one [double room](#quarto-zambujeira), one [triple room](#quarto-abelheira), one [terrace room](#quarto-do-terraco) for 2-4 persons, one [four-bed room](#quarto-azul), one [seven-bed room](#quarto-vermelho) and three separate [wooden houses](#tiny-house-luna) (Tiny Houses). In total we have space for 32 people. But this is distributed quite well, as there are many corners and niches in which one can retreat, for example if one wants to read a good book in peace.
