---
tags: bedAndBreakfast
partial: caravan-m-m
---
**Caravan M & M** is a cosy caravan with space for 2 people. The prices for this are exactly the same as for our shared rooms. For two people, this has the advantage that they are alone in the caravan and do not have to pay the double room supplement. The caravan has an electricity connection, the built-in toilet may not be used. Of course, guests may share the bathroom and kitchen in the main house, as well as the entire outdoor area.

Click here for [booking]({{ links.en.onlineBooking.path }}).
