---
tags: bedAndBreakfast
partial: main-house
---

## The rooms in the main house

The double room, the 3-bed room and the terrace room are on the first floor of the main house and share a bathroom with shower on the ground floor, while the terrace room also has a private toilet. The Girls-4-Bedroom is located on the ground floor of the main house next to the cosy fireplace room. The single beds can only be booked by women. But of course all other genders are also allowed if you book the entire room. The 7-bed room is located in an annex next to the kitchen. The shared rooms each have their own shower room. The 7-bed room also has a warm outside shower and offers a small kitchenette with fridge.

The communal kitchen, where breakfast is served every morning, is located on the ground floor next to the large covered outdoor area, where the barbecue evenings are also held.
