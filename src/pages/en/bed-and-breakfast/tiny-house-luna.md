---
tags: bedAndBreakfast
partial: tiny-house-luna
---

Since spring 2019, we are pleased to offer you our Tiny House “Luna” – with everything one needs. A small kitchenette, fridge, bathroom with toilet and shower. A quiet area with a beautiful view. Enjoy!

The mattress on the loft bed measures 140 x 190cm.

Prices per person and night including breakfast and 2x BBQ per week:

- July to September: 50 EUR
- October to June: 40 EUR
- Minimum occupancy: 2 persons (adults)
- Children up to 12 years 50% discount
- Babies up to 2 years free of charge

Click here for [booking]({{ links.en.onlineBooking.path }}).
