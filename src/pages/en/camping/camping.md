---
tags: camping
partial: camping
headline: No Glam / Just Camping!
subtitle: ""
---

For those who like to travel thrifty, we have the perfect option: camping! It is particularly inexpensive if you bring your own tent. Alternatively, we also offer pitches with electricity for mobile homes and caravans.

Our brand new outdoor kitchen and a modern bathroom will be available to you from 2023. We have built these facilities especially for you, so that you can make your stay with us even more pleasant and comfortable. Enjoy the feeling of freedom and independence while camping without having to forego the comfort of a kitchen and a modern bathroom and be in the middle of surf camp life.


* Overnight stay in your own tent, mobile home or caravan: EUR 10 per person per night (children up to 2 are free and children up to 12 pay half)
* Pitch fee for mobile homes or caravans: EUR 10 per night incl. electricity
* All prices include shared use of bathroom and outdoor kitchen
* NOT included in the price are the daily breakfast buffet (7 EUR) and our BBQ (13 EUR, Sunday and Friday). You are welcome to book this as an additional service and make your camping experience even more unforgettable.


The next disposal station is only a stone's throw away from us. It is located at the Intermarché supermarket in Lourinhã and can be reached in just 5 minutes. We also offer you the opportunity to fill up your fresh water tank with us.