---
permalink: /en/camping/index.html
layout: camping.njk
tags: camping
locale: en
navigation: Camping

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Camping at the Da Silva Surfcamp - Westcoast Portugal"

# max 158 characters
metaDescription: "Camping at the Da Silva Surfcamp"
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/camping.jpg"
---
