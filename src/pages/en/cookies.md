---
permalink: /en/cookies/index.html
layout: plain-text.njk
tags: cookies
locale: en
navigation: Cookies

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Cookie"

# max 158 characters
metaDescription: "Cookie | Privacy Policies"
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/surf-mtb.jpg"
---

# Cookies

We use 2 types of cookies: marketing cookies and functional cookies.

Checkout why we use cookies, and learn how you can accept or reject them.

## Marketing cookies

Marketing cookies help us understand how you found us. This allows us to better coordinate our marketing campaigns. The cookies are evaluated by Google Analytics. For more details checkout our [Privacy Policies](en/privacy-policies/).

If you do not want marketing cookies, press the button "Reject Marketing Cookies" on the **cookie consent banner** at the bottom of the page. Else press the "Accept Cookies" button.

## Functional cookies

Functional cookies are cookies that are necessary for technical reasons. We have 2 cookies on our site: one for videos and one for online booking.

### video cookies

The video cookie is set when you click on a video. We use the video player from "Vimeo", which needs the cookie to know when the video player is loaded and ready to play the video. You can find more details here: [Vimeo Cookie Policy](https://vimeo.com/cookie_policy)

If you don't want a video cookie, don't click on a video.

### Online booking cookie

When you go to Online Booking, a cookie is used. We use the service provider "Bookinglayer" to handle the booking and payment. This service provider can only be used with cookies. You can find more details here: [Booking Layer Cookie-Policy](https://www.bookinglayer.com/cookie-policy).

If you do not want to have this booking cookie, you can use our [booking request form](/en/booking-request/) or [contact](/en/contact/) us directly.
