---
tags: eBike
partial: e-bike-season-prices
---

### per E-Bike incl. helmet

- Low Season
  - (April - June)
  - 1 day: 25 €
  - 3 days: 70 €
  - 5 days: 105 €
- High Season
  - (July – October)
  - 1 day: 30 €
  - 3 days: 80 €
  - 5 days: 125 €
