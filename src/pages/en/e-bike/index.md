---
permalink: /en/e-bike/index.html
layout: e-bike.njk
tags: eBike
locale: en
navigation: E-Bike

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Entdeckt die Atlantikküeste mit dem E-Bike"

# max 158 characters
metaDescription: "Die portugiesische Landschaft und Atlantikküste läßt sich hervorragend mit dem E-Bike geniessen."
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/e-bike.jpg"
---
