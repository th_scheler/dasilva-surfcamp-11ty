---
permalink: /en/faq/index.html
layout: plain-text.njk
tags: faq
locale: en
navigation: FAQ

title: "FAQ - Frequently Asked Question"

metaImage: ""
metaDescription: "FAQ - Frequently Asked Questions"
---


# FAQ

#### Frequently Asked Question


### Should I rather take the beginners or the advanced class?

Beginner and advanced students are in the same class. We experienced that it works out very fine like that. Our surf instructors give individual feedback to every student, so everyone will be served at his needs. At a certain point a surf instructor guides you to be in the right position. Then it is mostly: practice, practice, practice…

### What is the average age at the camp?

This question is no longer so easy to answer, as we have specialized more and more in families with children. The parents are mostly in their mid-thirties and late forties. All ages are represented among the children. Some of the children come to us for the first time with their parents and when they are teenagers, they come to visit us again and bring friends of the same age with them. We welcome all ages, but we are definitely not a party camp, although we do have a dance floor or two in our bar at the weekend :-)

### Where is the closest airport? How do I  arrive at the surf camp?

The nearest airport is in Lisbon, about 70 km from our camp. To get to the bus station, you can either take the subway, Uber, or taxi. The express bus ride from Lisbon to Praia da Areia Branca costs EUR 8.90 per person. We also offer you an airport transfer (from 80 EUR).

### What about the catering?

At Da Silva Surfcamp a delicious breakfast buffet is offered every morning and two times a week there will be a joint dinner (barbecue). In all other evenings you can prepare the dinner by yourself in our kitchen of the surf house. Most times the students living in the camp cook together and share the cost. Or they take turns. Many reasonably priced restaurants are located in Praia da Areia Branca so that you need not cook every night in the surf house.

### On what day do the surf classes start?

The surf classes always start on Monday. If the wave- and weather conditions are optimal, the classes will take place every day so that the course will finish on Friday. But unless the conditions are acceptable, some classes may have to be cancelled and held during the weekend. This will be arranged very flexibly by the surf instructors on site.

### In which language will the classes take place?

Our surf instructors speak Portuguese and English. This won’t, however, be a problem at all as there are mainly practical classes. Most things will be demonstrated and explained accordingly through gestures. We have never faced communication problems between instructors and students.
