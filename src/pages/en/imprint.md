---
permalink: /en/imprint/index.html
layout: plain-text.njk
tags: imprint
locale: en
navigation: Imprint

title: "Imprint"

metaImage: ""
metaDescription: "Imprint"
---

# Imprint

### Owner/ Publisher

```
Daniel Wohlang da Silva
Da Silva Surfcamp Portugal
Casal da Capela, Casal da Murta
Rua Pôr do Sol, N° 21
2530 – 077 Lourinhã - Portugal
Mobile phone: +351 913 818 750
(Call / WhatsApp / Signal)
Telephon (landline): +351 / 261 461 515
E-Mail: surfcamp[@]dasilva.de
```
<br/>
Responsible according to § 55 Abs. 2 RStV: Daniel Wohlang da Silva


### Copyright

The content and works created by the site operator are protected by copyright. This affects the design as well as the content. Reproduction, processing or changes are only permitted after written approval. Likewise, all rights for reproductions, in whatever form and in whatever medium, are reserved.

### Disclaimer

Own content | All information is updated regularly and carefully, to the best of our knowledge and belief. In the legal sense, however, they are not guaranteed.

Foreign content In some places we refer to external sites (links). We have no influence on their design, content, topicality, completeness and correctness. We therefore expressly distance ourselves from all content on these pages, as well as from all pages to which the linked pages lead.

# Licences

### Font Awesome

We use some social icons and other playful icons from **Font Awesome** with the **Creative Commons Attribution 4.0 International** license and requires that we link to the [Font Awesome Licens](https://fontawesome.com/license). Thank you for the wonderful icons. We pretty much appreciate.
