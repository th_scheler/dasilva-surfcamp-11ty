---
permalink: /en/info/index.html
layout: plain-text.njk
tags: info
locale: en
navigation: Info

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Info"

# max 158 characters
metaDescription: "Important info to enjoy your holidays"
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/surf-mtb.jpg"
---

# Holiday Info

<div class="h2">Welcome to the
<br/>Da Silva Surfcamp</div>
<div class="h2">Olá e bem vindo!</div>

We are happy to welcome you as our guests. You can look forward to relaxed days full of sunshine and great waves.

The camp team is always there for you - we are ready to answer your questions, give suggestions or help to solve any issues that might occur. It is our job to make your holidays become the way you want it to be!

On the following pages you will find a short summary of important facts to make your stay as pleasant as possible for everyone on the camp site.

**We wish you a wonderful stay with us!**

### Your happiness

Our greatest wish is that you are pleased with everything you do here and that you gather fond memories of your stay. Just talk to us if something or another is not going that well, and we will do our best to change that as quickly as possible.

### Breakfast

Every morning, a fresh, healthy breakfast awaits you in the kitchen of the main building. The breakfast times depend on the pick-up of the surf school. The general rule is - breakfast one hour before departure. This applies to all. (If the pick-up is very early, breakfast will of course be left for everyone who is still on site). Once everyone is through, breakfast is cleared away by us, but no later than 10 am. However, we ask you to help us with the general cleanliness in the kitchen - your dishes and cutlery will go into the dishwasher, used pots and pans will be rinsed independently after the meal.

### Refrigerators

During your stay we serve a delicious breakfast every day and on two evenings of the week (Sunday and Friday) we organize an extensive BBQ together. We ask you not to use the breakfast buffet to make a lunch package for the rest of the day, as we don't calculate that way, and everyone should get their fill from breakfast ;) For your further catering, you can use the guest fridges in the main building as well as the compartments in the large shelves. These are labelled with the room names. If you are on site without a car - no problem, we will gladly drive you to the supermarket in Lourinhã, just ask us. In Praia da Areia Branca you will also find a small supermarket, a fruit shop and even a small market to the right. If you want to go out for a meal or order something, just ask us - apart from recommendations, we can also give you a ride.

### Flies and mosquitoes

Unfortunately, there are a lot of flies here. It is therefore essential that all doors and windows that are not fitted with fly screens are kept closed at all times.
The same applies to mosquitoes. Especially at dusk, all windows and doors must be absolutely closed. Unfortunately, some mosquitoes do manage to get into the bedrooms and cause sleepless nights for our guests.

### Rest periods

Surfing is exhausting - some of you already know that, some of you will soon realise it. Cosy rounds should not be interrupted, but please keep in mind that we start our bedtime routine at 23:00. Please be considerate if people want to go to bed earlier.

### Towels

Each guest will receive one large and one small towel upon arrival. These are automatically changed by us on Thursdays and Sundays. If for any reason you need a fresh towel in between, please let us know. Please do not take 
these towels to the beach! You should have your own bath towels.

### Bed linen

The beds are freshly made on arrival. If you are staying with us for longer than a week, you can get fresh bed linen. Just ask us for this.

### Key

Each room has a key which is in the lock when you arrive. In the Tiny Houses there is a key box at the front. If possible, leave your room open on Thursdays and Sundays so that we can put fresh towels in it and give it a good vacuuming.

### Washing machine

If you want to do your laundry, you are welcome to use the washing machine in our laundry room. We charge 5 EUR per machine including detergent. If this is not worth it for one person, you are of course welcome to pool to get one washing machine full.

### Laundry drying

There are several spots on site where you can hang your beach towels and wetsuits to dry.

### Tidiness and cleanliness

Please make sure that you leave everything as you found it. We ask you to wash used crockery, cutlery, pots and pans yourself or to use the dishwasher. The same applies to empty bottles and glasses - please put them away the same evening - the staff will thank you ;)

### Cigarettes

Smoking is only allowed outside, but please make sure you don't drop any butts on the floor - firstly because of the environmental impact (which is immense even with just one butt!), and secondly because there is no second person to pick them up. Besides, even a carelessly discarded cigarette can start a fire in the dry summer months.

### Shuttle bus

For the surf course you will be picked up every morning by the RIPAR surf school and brought back after surfing. Of course, you can continue to spend time on the beach afterwards if you wish to.

Depending on how it fits in with our schedule, we can also drive you or pick you up from somewhere. Please just ask us about this. Otherwise, you can get from A to B cheaply with UBER in Portugal :)

### Leisure activities

...are almost unlimited: Go-karting, horse riding, tennis, hiking, kiting, Dinopark and much more. We know the area pretty well and can give you lots of tips. We certainly don't want you to get bored with us! Just ask us about it and we will organise exciting excursions together with you. We are also happy to do many things together with you.

### Departure by rede-express bus

You should buy a bus ticket at the bus station in Lourinhã or online at least one day before departure. It is quite possible that the express bus is fully booked already and you will not be able to take the bus at desired time. Please make sure that the ticket goes from Praia da Areia Branca to Lisbon and not from Lourinhã. To pay for the ticket you need a credit card or paypal (if you book it online). You can get the tickets on this webpage: rede-express.pt.

### Beach cruiser

You are welcome to borrow one of our beach cruisers, just let us know. You are completely independent and can go to the beach, go shopping in Lourinhã or go out in the evening to Areia Branca at any time. Please remember to always take a lock with you and lock the bike to a fixed object. If you are out and about after sunset and need a light, please contact us. If you no longer need the bike, please put it back in the bike shed and please do not leave the bikes at the surf lodges, so that the bikes are available to every guest.

Lots of information, but if you have any questions, we are always there for you, just talk to us or contact us in advance via WhatsApp or Signal: 351 / 261 461 515

**Hang Loose**

**Your Da Silva – Surfcamp Team**