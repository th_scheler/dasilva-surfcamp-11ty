---
tags: mountainBike
partial: mtb-package
---

## MTB-Package

<div class="h3">from 475 € per week / person</div>

<div class="h4">including</div>

* 7 days bed & breakfast in our camp
* 2x barbecue (also vegetarian) with campfire
* 5 mountain bike tours (4-6 hours / 40-60 km per day)
* High-Level CUBE Fully Mountainbike (29 inches) with helmet
* Shuttle bus to the best MTB trails in our area
