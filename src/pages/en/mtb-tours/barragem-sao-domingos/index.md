---
permalink: /en/mtb-tours/barragem-sao-domingos/index.html
layout: mtb-tours-barragem-sao-domingos.njk
tags: mtbTourBarragemSaoDomingos
locale: en
navigation: Barragem Sao Domingos

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Mountain Bike Tours » Barragem Sao Domingos"

# max 158 characters
metaDescription: "The MTB tour goes through Barragem de São Domingos along the coast to Consoloção to the reservoir of Atouguia da Baleia Portugal"

# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/barragem-sao-domingos.jpg"
---
