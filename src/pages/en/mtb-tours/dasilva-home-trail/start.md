---
tags: mtbTourDasilvaHomeTrail
partial: start
---

# Da Silva Bikecamp<br>Hometrail

Varied house circuit through the hills northeast of Lourinhã and to the beach in Praia Areia Branca, with various technical passages and exciting scenery. Total duration with breaks: about 5 hours Difficulty: S0, with passages up to S1.
