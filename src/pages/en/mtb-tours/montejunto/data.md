---
tags: mtbTourMontejunto
partial: data
---

- Distance 39 km
- Moving time 4:15 h
- Elevation 1.213 m
- <a href="https://strava.app.link/WnzBsjcBLZ" target="_blank">![Strava link](/_assets/mtb-tours/strava.jpg)</a>
