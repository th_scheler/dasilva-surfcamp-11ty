---
tags: mtbTourMontejunto
partial: description
---

After one day of rest in camp, the big mountain climb on Montejunto awaits us today. The camp bus will take us to Vila Verde dos Francos, from where we will begin the approximately 10-kilometer climb to the summit.

On the north side of the Montejunto it is still a bit cooler in the morning and so the route, moreover on asphalt, drives itself quite smoothly in one through. Arrived on the summit we have at old monastery ruins at about 660 meters above sea level a great panoramic view and a fresh wind provide cooling.

From the summit, we make a detour to the mountain top at Penha do Meio Dia and from there we ride across the surrounding plateau towards the east. The trails here are stony and the vegetation rather sparse. Only with the descent, we reach more forest again. Daniel leads us cross-country over fast-changing trails and paths. It goes on sometimes under trees on over the soft forest floor, then again on hard, washed-out forest trails with deep grooves. The way downhill is long and extensive and we finally reach the valley on the south side of the mountain. The lush landscape is dominated by vineyards and is again completely different from all previous tours. At kilometer 25 we take a rest. Three steep climbs now await us on the south side of Montejunto. From about 130 meters above sea level, we climb again to 420 meters above sea level. It is hotter than during the first ascent and after the technical passages on the mountain, the forces are not quite there as they were in the morning. Depending upon need one should for this 
Tour at summer temperatures should have at least 3 liters of water and some snacks.

The climb rewards very beautiful views of the vineyards south of Montejunto, but really drains your strength. From the high point of this stretch, we descend steeply until we reach the vineyards. With the last reserves, we climb another 115 meters of altitude to then enjoy the descent on the trails.

On the way back to Vila Verde dos Francos, we do a few laps of honor around a historic windmill before rewarding ourselves, arrived in the village, with ice-cold Superbock 0.0 and a bag of chips.

Montejunto was, just from the condition, the hardest tour of the week. Who brings good fitness and pays attention to the food on the road (2.5 liters of water, an apple, a banana, crackers, and two cheese sandwiches were only just enough), should also pack the mountain classification well.
