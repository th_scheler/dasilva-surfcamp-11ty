---
permalink: /en/mtb-tours/montejunto/index.html
layout: mtb-tours-montejunto.njk
tags: mtbTourMontejunto
locale: en
navigation: Montejunto

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Mountain Bike Tours » Montejunto"

# max 158 characters
metaDescription: "Lonely MTB single trails lead through an untouched rugged mountain landscape of the Serra de Montejunto in Portugal"

# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/montejunto.jpg"
---
