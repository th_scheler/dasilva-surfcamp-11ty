---
tags: mtbTourSantaCruz
partial: data
---

- Distance 34 km
- Moving time 3:22 h
- Elevation 702 m
- <a href="https://strava.app.link/kQdmMHOCHZ" target="_blank">![Strava link](/_assets/mtb-tours/strava.jpg)</a>