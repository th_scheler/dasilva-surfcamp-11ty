---
permalink: /en/mtb-tours/santa-cruz/index.html
layout: mtb-tours-santa-cruz.njk
tags: mtbTourSantaCruz
locale: en
navigation: Santa Cruz

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Mountain Bike Tours » Santa Cruz"

# max 158 characters
metaDescription: "MTB tour along Portugal's cliffs with an great sea view | Praia Areia Branca | Ribamar | Vimeiro | Santa Rita | Santa Cruz"

# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/santa-cruz.jpg"
---
