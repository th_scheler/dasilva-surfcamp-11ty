---
tags: mtbTourSintra
partial: data
---

- Distance 35 km
- Moving time 3:45 h
- Elevation 1107 m
- <a href="https://www.strava.com/activities/1729081720" target="_blank">![Strava link](/_assets/mtb-tours/strava.jpg)</a>
