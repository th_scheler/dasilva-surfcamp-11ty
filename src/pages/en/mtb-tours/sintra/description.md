---
tags: mtbTourSintra
partial: description
---

With the camp bus, we take the highway in the direction of Lisbon. After about an hour's drive, we reach São Pedro de Penaferrim, a district of Sintra. As a small booster, we have a quick one Bica with Pasteis de Nata.

Through the town, we go straight up some steep roads, but luckily it doesn't take too long until we reach the forest and the road levels out a bit. After yesterday's tour, where there was hardly any shade, the start today under trees is really pleasant. How close we are to the palace of Pena (and the countless tourists) is not noticeable.

On graveled forest paths we reach Lagoa Azul, a small lake at the edge of which turtles are actually "sunbathing". The vegetation here is so different from the first two tours that you almost think you are in a different country.

A narrow trail leads us along the country road before the first and very long climb starts. The surface is grippy, but the slope is steep. As a reward, we ride a never-ending trail with nimble curves, small bridges made of logs, and a fantastic flow. Once you reach the bottom, you can't help but grin.
