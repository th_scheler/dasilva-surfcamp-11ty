---
permalink: /en/mtb-tours/sintra/index.html
layout: mtb-tours-sintra.njk
tags: mtbTourSintra
locale: en
navigation: Sintra

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Mountain Bike Tours » Sintra"

# max 158 characters
metaDescription: "Fantastic MTB flow trails and challenging passages in the World Heritage Site around the Palácio Nacional da Pena Portugal"

# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/sintra.jpg"
---
