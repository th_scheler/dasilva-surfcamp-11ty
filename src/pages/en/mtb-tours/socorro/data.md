---
tags: mtbTourSocorro
partial: data
---

- Distance 42 km
- Moving time 4:35 h
- Elevation 1.169 m
- <a href="https://strava.app.link/LCL8AVXoNZ" target="_blank">![Strava link](/_assets/mtb-tours/strava.jpg)</a>
