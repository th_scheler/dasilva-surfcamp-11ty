---
permalink: /en/mtb-tours/socorro/index.html
layout: mtb-tours-socorro.njk
tags: mtbTourSocorro
locale: en
navigation: Socorro

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Mountain Bike Tours » Socorro"

# max 158 characters
metaDescription: "Demanding tour with downhill passages (S2-S3) near Torres Vedras Portugal | Cucos | Serra do Socorro"

# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/socorro.jpg"
---
