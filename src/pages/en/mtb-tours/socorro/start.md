---
tags: mtbTourSocorro
partial: start
---

# Cucos – Serra do Socorro

Challenging tour with downhill passages (S2-S3) in the area of Torres Vedras, about 25 km south of our camp.
