---
tags: prices
partial: bedAndBreakfastPrices
---

- Low Season
  - (Oct. – June)
   - Multiple: 25 €
  - Double: 30 €
  - Suite: 35 €
  - Tiny House: 40 €
  - Single: 55 €
- High Season
  - (July – September)
  - Multiple: 35 €
  - Double: 40 €
  - Suite: 45 €
  - Tiny House: 50 €
  - Single: 65 €

<div style="text-align: center">
Rental of high-quality CUBE <a href="https://www.dasilva-surfcamp.de/en/e-bike/" target="_blank">E-Bikes</a> from € 25 per day<br>
</div>
