---
tags: prices
partial: bedAndBreakfast
---

# Our Surf Holidays Prices

### Bed & Breakfast

## from 25 € per day / person

#### including:

- accommodation & daily breakfast buffet
- 2x BBQ per week (also vegetarian) for 1-week stay or more
- daily shuttle service to our beach
- free use of beach cruisers
- free WIFI on the property
