---
tags: prices
partial: directions-tips-activities
---

## Arrival and departure

In theory, you can arrive and depart from us on any day of the week. Since the surf courses always start on Monday and we do our welcome BBQ on Sunday evening. So it is advisable to arrive and depart on Sunday, also because of the group dynamics that develop automatically over the course of a week.

Apropos, this is  how to get to us: [Arrival]({{links.en.travelDirectionspath}}).

## Beachruiser

For the way to the beach or just to drive around, you can borrow chic beach cruisers from us free of charge at any time. That's cool and loosens your legs: o)

## Food

The daily breakfast buffet can be found in the surf camp kitchen and can be enjoyed outside or in the living room. The BBQs, which take place twice a week, always take place outside on sundays and fridays. When preparing breakfast and BBQs, we always ensure there is plenty of vegetarian and vegan options.

## Other food options

In the beach town of Praia da Areia Branca (2 km) there is a small supermarket and a daily weekly market with fresh fruit and vegetables, fish, etc. You can actually get everything you need there. In the district town of Lourinha (4 km) there are larger supermarkets (e.g. also LIDL and ALDI), where there is an even larger selection and which are open every day until 9 p.m. If you don't have your own car, our team members will also take you with them to go shopping. Apart from that there are plenty of cheap cafes and restaurants everywhere, where you can get delicious typical Portuguese food. But there is also a pizzeria, a particularly good Indian restaurant right on the beach and a fast-food snack bar on the main road. Otherwise there are also different possibilities to order food directly to us in the surf camp. Our teamers are also happy to make a collective order for all guests and then pick up the food for you.

