---
tags: prices
partial: surf-mtb-package-about
---

## Surf & MTB Package

### from 475 € per week / person

The combination of surfing with mountain biking! Ideal for every surfer who wants to go mountain biking while on holiday, and for every mountain biker who wants to surf while on holiday. The good thing about it: You can set the schedule so that you can surf when the waves are good and on the other days you can go on the MTB trails. 3 days surfing and 2 days mountain biking or the other way around, could you possibly do more in one week?
