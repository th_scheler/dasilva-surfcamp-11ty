---
tags: prices
partial: surf-mtb-package-including
---

<div class="h4">including</div>

* 7 days bed & breakfast at Da Silva Camp
* 2x barbecue (also vegetarian) with campfire
* 5 days of action with surfboard or mountainbike
* 2 or 3 days of surf lessons (2 hours per session / 2 sessions per day) complete surf equipment (surfboard, leash, wetsuit)
* Shuttle bus to the beaches with the best waves in our area
* 2 or 3 mountain bike tours (4-6 hours / 40-60 km per day)
* High-level CUBE fully mountain bike (29 inches) with helmet
* Shuttle bus to the best MTB trails in our area
  
