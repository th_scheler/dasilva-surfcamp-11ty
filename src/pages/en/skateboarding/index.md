---
permalink: /en/skateboarding/index.html
layout: skateboarding.njk
tags: skateboarding
locale: en
navigation: Skateboarding

title: "Skateboarding Miniramp Skatepark"

metaImage: "/_assets/teasers/skateboarding.jpg"
metaDescription: "Surfing, Biken and much more"
---
