---
tags: surfLodges
partial: casa-vasco
---

Goreti Vasco has lived in Germany with her family for many years. Now she has fulfilled her dream of owning a house in Portugal and rents two holiday apartments 400m from our surf camp. She is happy about guests with whom she can converse in German. If you take a leisurely stroll along the country lanes from here, you can reach the nearby bathing beaches in a short time. Guests can use the inner courtyard and a small meadow with a view of the countryside. The apartment consists of a large kitchen-living room with fireplace and TV, two double bedrooms (each 135 x 185 cm) and a bathroom with bathtub. There is also a second kitchen with a fireplace where you can have a good barbecue.

<b>Prices per night including breakfast and 2x BBQ per week at the surf camp:</b>

- for 2 people: 110 EUR
- for 3 people: 135 EUR
- for 4 people: 160 EUR
- Minimum occupancy: 2 persons (adults)
- Babies up to 2 years free of charge

Click here for [booking]({{ links.en.onlineBooking.path }}).