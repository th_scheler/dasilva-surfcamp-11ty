---
tags: surfLodges
partial: da-silva-surf-lodges
---

We offer the "Da Silva Surf Lodges" as an exclusive alternative to our surf camp. Only 300m away from us, you can enjoy your holiday in an idyllic ambience. Five apartments are available with one bedroom, a spacious living area with an open kitchen and a comfortable bathroom. The sixth apartment has two bedrooms and two bathrooms (price on request). Smart TV, W-Lan, dishwasher are a matter of course. In front of the building there is a small lake with a natural spring. The wooden gazebo over the lake is great for reading a good book while listening to the birds chirping :)

**Prices per person and night including breakfast and 2x BBQ per week:**

**July to September**

- for 2 people: 60 EUR
- for 3 people: 50 EUR
- from 4 people: 40 EUR

**October to June**

- for 2 people: 50 EUR
- for 3 people: 40 EUR
- from 4 people: 35 EUR

**Conditions**

- Minimum occupancy: 2 persons (adults)
- Babies up to 2 years free of charge

Click here for [booking]({{ links.en.onlineBooking.path }}).
