---
tags: surfLodges
partial: welcome
---

# Da Silva Surf Lodges

## Holiday Homes, Apartments & Studios

### for individual surfing holidays in Portugal
<br>
<p>In the vicinity of our surf camp there are various holiday homes, apartments and studios that are ideal for a surfing holiday. The quiet location, a comfortable living room with terrace and the well-equipped kitchen, in which you can cater for yourself, enable an individual surfing holiday here. You don't have to do without the amenities of a surf camp.<p/>

All of our sports offers, such as surfing courses, mountain biking, pony riding, yoga and trail running can be booked. Apart from that, you can come and join us at any time. Our barstuff is happy about every visit and the kids can jump into the pool while the parents refresh themselves at the bar. It is not uncommon for the conversations started there to end around a romantic campfire. The price includes two BBQs per week and a filled fridge for the daily breakfast or, if you wish, the breakfast buffet in the surf camp.