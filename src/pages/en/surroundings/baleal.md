---
tags: surroundings
partial: baleal
mapLink: https://goo.gl/maps/n6XtzJkkn3iUbPgF8
---

The surf mecca of Europe (12 km north) with over 40 surf camps and nice nightlife. A bit crowded, but always worth a visit.
