---
tags: surroundings
partial: museu-lourinha
webLink: https://museulourinha.org
mapLink: https://bit.ly/2D59BjA
---

## Museu da Lourinhã

Would you like to travel back in time? Then learn the exciting story of the area around Lourinhau. Including dinosaur skeleton, the life and work of people, and fascinating craftsmanship from various eras.
