---
tags: surroundings
partial: nazare
mapLink: https://goo.gl/maps/CqsQGckf1wFVMJVJA
---

The world-famous monster waves can only be seen here (60km north) out of season, but the place is also very beautiful and absolutely worth seeing.
