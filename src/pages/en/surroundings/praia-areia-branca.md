---
tags: surroundings
partial: praia-areia-branca
mapLink: https://goo.gl/maps/SvoMUacFBzMmB7bj7
---

Our "home spot" (2 km, 10 min. by bike) with beach promenade, good waves and great nightlife. The express bus from Lisbon also stops here.
