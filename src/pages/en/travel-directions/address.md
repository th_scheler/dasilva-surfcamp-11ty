---
tags: travelDirections
partial: address
---

# Arrival

Arriving from the airport in Lisbon is really easy! You can take a Taxi/Uber or the subway to the main bus terminal and from there you’ll take the express bus, that will drop you off close to our surf camp. We are happy to pick you up at the bus station and take you to the camp.

## Address

Da Silva Surfcamp<br>
Casal da Capela, Casal da Murta<br>
Rua Pôr do Sol, N° 21<br>
2530 – 077 Lourinhã<br>
Google Maps: <a href="https://g.page/DaSilvaSurfcamp">https://g.page/DaSilvaSurfcamp</a><br>
Mobile phone: <a href="tel:+351913818750">+351 913 818 750</a><br>
Phone(landline): <a href="tel:+351261461515">+351 261 461 515</a><br>