---
tags: travelDirections
partial: directions
---

## General Information

The nearest airport is in Lisbon, about 65 km south of us. From there you can easily and cheaply take the express bus directly to Praia da Areia Branca, where we will be happy to pick you up from the bus stop. Otherwise you can also hire a car at the airport, take a taxi or use our airport transfer. Arrival and check-in at the Surfcamp is on Sundays and starts at 4 pm and departure is until 11 am. Before and after you can of course put your luggage with us and enjoy the time with us.

## Expressbus

The express bus departs from the "Sete Rios" bus station. To get to the bus terminal, you can either take the subway, Uber, or taxi.

**Subway:** After your arrival, leave the airport and turn right in the direction of the subway. At the ticket machine you buy a rechargeable ticket (50 cents) per person with one trip (1.10 EUR). Then you take the red line "Vermelho" to "Sao Sebastiao", change to the blue line "Azul" towards "Reboleira" and get off at the stop "Jardim Zoologico".

**Uber:** After your arrival (ground floor) you have to go to the "Passenger Drop Off" in front of the departure hall (upper floor) because Uber is only allowed to stop here. Here you can take an Uber to the bus station or you can let yourself be driven directly to the surf camp.

**Taxi:** Here you have to be careful not to be scammed. The ride should not cost more than 10 EUR. Reccomendation: Don't take the taxis that are waiting at Arrivals, but go to Departures and take a taxi there.

Arrived at the bus station "Sete Rios" you have to buy a ticket to Praia da Areia Branca at the counter before boarding the express bus with final destination "Peniche". You can also buy the tickets online under this link or download the very practical app with which you can save the bus tickets on your smartphone: <a href="https://www.rede-expressos.pt/" target="_blank">rede-expressos.pt</a>

The journey by express bus from Lisboa to Praia da Areia Branca costs 8.90 EUR and takes exactly 1 hour and 15 minutes. Final destination is Peniche. Praia da Areia Branca is the penultimate stop and not a proper bus station, just a small bus stop in the middle of Praia da Areia Branca. If you have passed through the district town of Lourinha, you have to get off at the next stop.

Best you try to take the bus that arrives in Praia da Areia Branca at 17:00 or 18:15. Then you are just in time for the Welcome-BBQ, which takes place every Sunday.

## Airport Transfer

If you would like to have a personal service, we will be happy to pick you up at any time from the airport in Lisbon or bring you there. This is particularly useful if your flight takes place outside of the express bus travel times. We welcome you in the arrivals area at Starbucks. You can recognize us by the Da Silva t-shirt. We are happy to stop at a supermarket (30 minutes) on the way to the surf camp so that you can stock up on the most important things for the first few days. Our shuttle bus can accommodate up to 8 people. Please be sure to register and confirm the airport transfer with us in good time.

```
Price for 1 - 4 people = 80 EUR
Price for 5 - 8 people = 120 EUR
+ 20 € night surcharge (from 01:00 to 05:00 a.m.)
```

## Arrival by car

If you are travelling by car using a navigation device, **please DO NOT navigate to our postal address**! There are **several streets with the same name** in our area and you are therefore often navigated to the wrong address. The safest way to find us is with **Google Maps: “Da Silva Surfcamp”**.

## Short Stopover in Lisbon?

Would you like to explore beautiful Lisbon before or after your stay with us? Then we can absolutely recommend the hostel <a href="https://www.lisboncalling.net/" target="_blank">Lisbon Calling</a> to you. It's in a great location, just a 3 minute walk to the trendiest district "Cais Sodré" and just a 10 minute walk to "Bairro Alto", where there's one cool pub next to the other. In addition, the people who work there are extremely nice and the hostel is really nicely furnished. It's best to have a look at the homepage and get your own impression :-)

And if you want it a little quieter and with your own bathroom, you are in better hands in the new accommodation <a href="https://www.lisboncalling.net/double-rooms-studio/" target="_blank">Lisbon Calling in the Rua da Fé</a>.
