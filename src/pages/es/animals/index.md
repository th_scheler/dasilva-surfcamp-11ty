---
permalink: /es/animals/index.html
layout: animals.njk
tags: animals
locale: es
navigation: Animals

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Animals"

# max 158 characters
metaDescription: "Pferde und Ponnies reiten | Mensch und Pferde haben gemeinsam Freude |  Kinder und Eltern im Surf Urlaub in Portugal"
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/animals.jpg"
---
