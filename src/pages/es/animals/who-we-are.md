---
tags: animals
partial: who-we-are
---

Wir, das bin ich, **Christiane**, die als kleines Mädchen bereits ihr Herz an die Pferde verloren hatte und seitdem den Traum vom eigenen Pferd träumte. Nach einem künstlerischen beruflichen Werdegang, der als Szenenbildnerin beim Film mündete, habe ich gemerkt, dass vor lauter Arbeit etwas Wesentliches auf der Strecke geblieben ist, und habe mir die Pferde zurück in mein Leben geholt. Seitdem weiß ich, was eigentlich immer gefehlt hat.

Meine Liebe zu Pferden hat sich bereits im Kindesalter seinen Weg gebahnt, und so bin ich sehr glücklich nun endlich viel Zeit mit meinen eigenen Ponies und Pferden verbringen zu können. 

Irgendwie war mir immer klar, dass die Pferde von Grund auf sehr liebe Tiere sind und was auch immer sie **tun**, sie es nicht **tun**, um uns weh zu **tun**. Es ist ja eher ein Wunder und ein Geschenk der Natur, dass diese großen Tiere sich uns bereitwillig  anschließen und gerne bei uns sind, wenn wir freundlich und gut mit ihnen umgehen. Diese Art des Umgangs und eine feine und pferdefreundliche Reitweise sind das, was ich mit meinen Ponys und Pferden heute Kindern, Jugendlichen und Erwachsenen vermitteln möchte. Und natürlich sollen Spaß und gute Laune dabei nicht zu kurz kommen.

**Jesuli**, mein Lusitano-Wallach, 7 Jahre jung und ein sehr feiner Kerl, der genau wissen will, mit wem er es zu tun hat. Mir hat er sofort ins Herz geschaut.

**Timól**, ein 9-jähriger Shetty-Mix, der aus einer sehr guten Ausbildung bei einem jungen Portugiesen zu mir gekommen ist. Er ist der absolute Schatz mit Kindern und ausser Reiten macht es auch total Spaß, mit ihm vom Boden zu arbeiten, zu spielen oder spazieren zu gehen.

**Eliott**, 10 Jahre schwarz-braunes Shetty Pony, stiehlt sich in alle Herzen mit seiner niedlichen Nase und seinem freundlichen Wesen.