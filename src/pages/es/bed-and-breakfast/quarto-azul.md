---
tags: bedAndBreakfast
partial: quarto-azul
subtitle: Girls 4-Bettzimmer
---

Unser 4-Bettzimmer im Erdgeschoss erreicht man direkt vom Kaminzimmer aus. Darin stehen zwei Etagenbetten und den Gästen steht ein eigenes Duschbad zur Verfügung. Direkt neben der Tür gibt es noch ein Gäste-WC im Kaminzimmer.

Preise pro Person und Nacht inkl. Frühstück und 2x BBQ pro Woche:

- Juli bis September: 30 EUR
- Oktober bis Juni: 25 EUR
- Belegung: 1-4 Personen

Hier geht es zur [Buchung]({{ links.de.onlineBooking.path }})
