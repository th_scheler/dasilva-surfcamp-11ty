---
tags: camping
partial: camping
headline: No Glam / Just Camping!
subtitle: ""
---

Para aquellos a los que les gusta viajar ahorrando, tenemos la opción perfecta: ¡acampar! Es particularmente económico si traes tu propia tienda de campaña. Alternativamente, también ofrecemos parcelas con electricidad para casas móviles y caravanas.

Nuestra nueva cocina al aire libre y un baño moderno estarán disponibles para usted a partir de 2023. Hemos construido estas instalaciones especialmente para ti, para que puedas hacer tu estancia con nosotros aún más agradable y cómoda. Disfrute de la sensación de libertad e independencia mientras acampa sin tener que renunciar a la comodidad de una cocina y un baño moderno y estar en medio de la vida del campamento de surf.


* Pasar la noche en su propia tienda de campaña, casa móvil o caravana: 10 EUR por persona y noche (los niños de hasta 2 años son gratis y los niños de hasta 12 pagan la mitad)
* Tarifa de parcela para mobil-homes o caravanas: 10 EUR por noche, electricidad incluida
* Todos los precios incluyen uso compartido de baño y cocina exterior
* NO están incluidos en el precio el desayuno buffet diario (5 EUR) y nuestra barbacoa (10 EUR, domingo y viernes). Le invitamos a reservar esto como un servicio adicional y hacer que su experiencia de campamento sea aún más inolvidable.


La siguiente estación de eliminación está a tiro de piedra de nosotros. Está ubicado en el supermercado Intermarché en Lourinhã y se puede llegar en solo 5 minutos. También le ofrecemos la oportunidad de llenar su tanque de agua dulce con nosotros.
