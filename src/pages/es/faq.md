---
permalink: /es/faq/index.html
layout: plain-text.njk
tags: faq
locale: es
navigation: FAQ

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "FAQ"

# max 158 characters
metaDescription: "Fragen zur Surfschule, Buchung, Unterkunft und Verpflegung uvm."
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/surf-mtb.jpg"
---


# FAQ

#### Häufig gestellte Fragen und ihre Antworten


### Soll ich lieber den Anfänger oder Fortgeschrittenenkurs machen?

Anfänger und Fortgeschrittene sind bei uns zusammen in einem Kurs. Wir haben die Erfahrung gemacht, dass das so sehr gut klappt. Unsere Surflehrer gehen natürlich auf das Können jedes Einzelnen genau ein, so dass jeder etwas davon hat. Ab einem gewissen Level (Fortgeschritten) kann ein Surflehrer einem sowieso nichts mehr beibringen. Da hilft nur noch üben, üben, üben…

### Wie ist das Durchschnittsalter bei Euch?

Diese Frage ist inzwischen nicht mehr so leicht zu beantworten, da wir uns immer mehr auf Familien mit Kindern spezialisiert haben. Die Eltern sind meistens zwischen Mitte Dreißig und Ende Vierzig. Bei den Kindern sind alle Altersstufen vertreten. Teilweise kommen die Kinder das erste Mal mit ihren Eltern zu uns und wenn sie im Teenageralter sind, kommen sie uns nochmal besuchen und bringen ihre gleichaltrigen Freunde mit. Bei uns sind alle Altersstufen willkommen, wir sind aber definitiv kein Partycamp, obwohl am Wochenende auch mal das eine oder andere Tanzbein in unserer Bar geschwungen wird :-)

### Wo ist der nächste Flughafen? Wie erfolgt die Anreise?

Der näheste Flughafen ist in Lissabon, ca. 70 km von unserem Camp entfernt. Um zum Busbahnhof zu kommen, könnt ihr entweder mit der U-Bahn, Uber oder Taxi fahren. Die Fahrt mit dem Expressbus von Lissabon nach Praia da Areia Branca kostet 8,90 EUR pro Person. Außerdem bieten wir Euch einen Airport-Transfer (ab 80 EUR) an.

### Wie läuft das mit der Verpflegung?

Im Da Silva Surfcamp gibt es jeden Morgen ein leckeres Frühstücksbuffet und 2x die Woche ein gemeinsames Abendessen (Barbecue). An den anderen Abenden könnt Ihr in der Küche unseres Surfhouses selber kochen. Meistens kochen die Leute, die im Surfhouse wohnen, zusammen und teilen sich die Kosten dafür. Oder es kocht mal der eine und mal der andere. Das wird alles vor Ort flexibel miteinander abgesprochen. Außerdem gibt es in Praia da Areia Branca einige sehr günstige Restaurants, so dass man nicht jeden Abend im Surfhouse kochen muss.

### An welchem Wochentag fangen die Surfkurse an (Kursbeginn)?

Die Surfkurse fangen immer montags an und wenn die Wellen- und Wetterkonditionen es zulassen, finden die Kurse täglich statt, so dass der letzte Kurs freitags abgehalten wird. Es kommt aber oft vor, dass aufgrund der genannten Konditionen der eine oder andere Kurs ausfallen muss und dann erst am Wochenende stattfindet. Alles in allem ist das ganze aber sehr flexibel und wird vor Ort und nach Absprache mit den Surflehrern oft individuell geändert.

### In welcher Sprache finden die Surfkurse statt?

Unsere Surflehrer sprechen nur portugiesisch und englisch. Das ist aber überhaupt kein Problem. Es handelt sich ja hauptsächlich um praktischen Unterricht, d.h. die meisten Dinge werden einfach vorgemacht, bzw. mit Händen und Füssen erklärt. Bisher hat es jedenfalls noch nie Kommunikationsschwierigkeiten zwischen den Lehrern und Schülern gegeben.

