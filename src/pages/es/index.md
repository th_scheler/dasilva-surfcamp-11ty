---
permalink: /es/index.html
layout: home.njk
tags: home
locale: es
navigation: Camp

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Surfing und Mountainbike und mehr"

# max 158 characters
metaDescription: "Unterkunft im portugiesischen Landhaus | Surf Schule | Mountainbike Touren | Natur, Strand und Pferde und viel Spass für Kinder"
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/surf-mtb.jpg"
---
