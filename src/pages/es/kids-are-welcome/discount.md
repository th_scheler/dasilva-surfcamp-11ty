---
tags: kidsAreWelcome
partial: discount
---

Preislich versuchen wir, den Eltern etwas entgegen zu kommen, in dem wir für Babys bis zwei Jahre gar nichts berechnen. Babybetten stellen wir kostenlos zur Verfügung und für Kinder bis 12 Jahre, die mit ihren Eltern im Zimmer auf einem Zustellbett schlafen, berechnen wir nur die Hälfte. Surfkurse bieten wir für Kinder ab 6 Jahre an, allerdings ohne Rabatt, da ein extra Surflehrer beschäftigt werden muss. Eltern und Kinder sind dann aber trotzdem zusammen in einem Kurs. Für Eltern mit Kleinkindern bieten wir an, dass sie sich einen Surfkurs teilen, damit immer einer von beiden auf das Kind aufpassen kann. Alternativ können wir auch einen Babysitter organisieren.