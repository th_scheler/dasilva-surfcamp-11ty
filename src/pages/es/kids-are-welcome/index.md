---
permalink: /es/kids-are-welcome/index.html
layout: kids-are-welcome.njk
tags: kidsAreWelcome
locale: es
navigation: Kids Welcome

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Kinder sind willkommen"

# max 158 characters
metaDescription: "Diversión para toda la familia | Surfear en la playa con mamá y papá o en el mar | Montar a caballo en el campamento | Muchos otros niños | family"
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/kids-are-welcome.jpg"
---
