---
tags: mountainBike
partial: bikes
---

## Unsere Mountain Bikes

In den Preisen für unsere Biketouren sind hochwertige vollgefederte CUBE Mountainbikes (Fully) enthalten. Es stehen die Modelle CUBE AMS, Sting und Stereo mit 29 Zoll Laufrädern und 120mm Federweg (vorn und hinten) zur Auswahl.