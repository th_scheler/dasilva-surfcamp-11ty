---
tags: mountainBike
partial: tours
---

## Unsere Touren

Wir fahren meistens 40-60 km und kommen dabei auf 800-1200 Höhenmeter. Der größte Teil davon ist eher Cross-Country, es ist aber auch immer etwas Enduro und teilweise sogar ein bisschen Downhill dabei. Das hängt vor allem von den Wünschen und Ansprüchen unserer Teilnehmer ab, nach denen wir uns gern richten. Es sollte aber immer für jeden etwas dabei sein. Die folgenden Touren dienen nur als Beispiele. Es gibt unzählige Varianten davon, die man hier auf <a href="https://www.strava.com/athletes/17635626" target="_blank">![Strava link](/_assets/mtb-tours/strava.jpg)</a>  sehen kann.

