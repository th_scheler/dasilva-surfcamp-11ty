---
tags: mtbTourDasilvaHomeTrail
partial: data
---

- Strecke 39 km
- Reine Fahrzeit 3:25 h
- Höhenmeter 624 m 
- <a href="https://strava.app.link/PmdAGJILEZ" target="_blank">![Strava link](/_assets/mtb-tours/strava.jpg)</a>
