---
tags: mtbTourMontejunto
partial: start
---

# Montejunto

Die „Serra de Montejunto“ liegt ca. 40 km südöstlich von unserem Camp und ist landschaflich besonders reizvoll. Einsame Singletrails führen hier durch unberührte raue Landschaft. Gesamte Dauer mit Pausen: ca. 5 Stunden Schwierigkeit: S0-S1, mit Passagen bis S2.
