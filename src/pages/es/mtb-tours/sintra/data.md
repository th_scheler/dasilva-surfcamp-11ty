---
tags: mtbTourSintra
partial: data
---

- Strecke 35 km
- Reine Fahrzeit 3:45 h
- Höhenmeter 1107 m
- <a href="https://www.strava.com/activities/1729081720" target="_blank">![Strava link](/_assets/mtb-tours/strava.jpg)</a>
