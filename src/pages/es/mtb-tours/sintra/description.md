---
tags: mtbTourSintra
partial: description
---

Mit dem Camp-Bus geht es über die Autobahn Richtung Lissabon. Nach etwa einer Stunde Fahrt erreichen wir São Pedro de Penaferrim, einen Ortsteil von Sintra. Als kleinen Anschub gibt es einen schnellen einen Bica mit Pasteis de Nata.

Durch den Ort geht es direkt ein paar steile Straßen rauf, aber zum Glück dauert es nicht zu lang, bis wir den Wald erreichen und sich der Weg etwas ebnet. Nach der gestrigen Tour, bei der es kaum Schatten gab, ist der Beginn heute unter Bäumen wirklich angenehm. Wie nah wir dem Palast von Pena (und den unzähligen Touristen) grade sind, ist nicht zu merken.

Auf geschotterten Waldwegen erreichen wir den Lagoa Azul, einen kleinen See an dessen Rand sich tatsächlich Schildkröten „sonnenbaden“. Die Vegetation hier unterscheidet sich so sehr von den ersten beiden Touren, dass man fast glaubt, in einem anderen Land zu sein.

Ein schmaler Trail führt uns ein Stück neben der Landstraße entlang, bevor der erste und sehr lange Anstieg wartet. Der Untergrund ist griffig, aber die Steigung hat es in sich. Zur Belohnung fahren wir einen nicht enden-wollenden Trail mit flotten Kurven, kleinen Stegen aus Rundhölzern und einem fantastischen Flow. Unten angekommen muss man einfach Grinsen.
