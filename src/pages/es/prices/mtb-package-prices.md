---
tags: prices
partial: mtb-package-prices
---

- Low Season
  - (October – June)
  - Multiple: 475 €
  - Double: 510 €
  - Suite: 545 €
  - Tiny House: 580 €
  - Single: 685 €
- High Season
  - (July – September)
  - Multiple: 510 €
  - Double: 545 €
  - Suite: 580 €
  - Tiny House: 615 €
  - Single: 755 €
