---
tags: prices
partial: surf-mtb-package-prices
---

- Low Season
  - (October – June)
  - Multiple: 475 €
  - Double: 510 €
  - Suite: 545 €
  - Tiny House: 580 €
  - Single: 685 €
- High Season
  - (July – September)
  - Multiple: 525 €
  - Double: 560 €
  - Suite: 595 €
  - Tiny House: 630 €
  - Single: 770 €
