---
tags: prices
partial: surfPackage2WeeksPrices
---

- Low Season
  - (October – June)
  - Multiple: 770 €
  - Double: 840 €
  - Suite: 820 €
  - Tiny House: 980 €
  - Single: 1.190 €
- High Season
  - (July – September)
  - Multiple: 870 €
  - Double: 940 €
  - Suite: 1.010 €
  - Tiny House: 1.080 €
  - Single: 1.360 €
