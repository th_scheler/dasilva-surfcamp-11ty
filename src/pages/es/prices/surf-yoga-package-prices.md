---
tags: prices
partial: surfYogaPackagePrices
---

- Low Season
  - (October – June)
  - Multiple: 515 €
  - Double: 550 €
  - Suite: 585 €
  - Tiny House: 620 €
  - Single: 725 €
- High Season
  - (July – September)
  - Multiple: 570 €
  - Double: 605 €
  - Suite: 640 €
  - Tiny House: 675 €
  - Single: 815 €
