---
tags: yoga
partial: start
---

## Mejora tu surf y bienestar a través del yoga

Encuentra con nosotros la desaceleración, el enfoque y la paz que necesitas para llegar completamente a ti mismo. Nuestras sesiones de yoga te ofrecen el equilibrio perfecto para el surf y fortalecen tu cuerpo para una sesión de surf exitosa.

El yoga y el surf están hechos el uno para el otro: mientras estás completamente en acción surfeando, encuentras la paz interior en el yoga.

Ofrecemos una mezcla de yoga clásico Hatha y Vinyasa moderno adaptado a todos los niveles. Ya seas principiante o un yogi experimentado, encontrarás las sesiones adecuadas con nosotros.

Queremos contribuir a promover tu salud y armonizar tu mente, cuerpo y naturaleza.
