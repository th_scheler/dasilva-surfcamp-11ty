---
tags: surfLodges
partial: casa-francisco
---

Casa Francisco liegt im gemütlichen Ort Casal da Murta auf der anderen Seite der Landstraße, die man überqueren muss, wenn man zu unserem Surfcamp möchte. In einer ruhigen Seitenstraße gelegen kommt man von hier über Feldwege zu wunderschönen einsamen Badebuchten (nur 1 km). Es stehen Gartenmöbel und eine Grillstelle zur Verfügung. Auf dem Dach gibt es eine große Terrasse mit schönem Blick über das Land. Die Wohnung hat zwei Schlafzimmer (mit einem Doppelbett und zwei Einzelbetten), Wohnzimmer mit Kamin, Fernseher und Internetzugang, Küche und Badezimmer. Die Besitzer wohnen im Haupthaus und sind sehr freundlich und hilfsbereit. Den Innenhof teilt man sich mit den Gästen des "Studio Prata" und "Quarto Por do Sol". Alle drei Unterkünfte zusammen bieten Platz für 8 Personen. 

<b>Preise pro Person inkl. Frühstück* und 2x BBQ pro Woche:</b>

- für 2 Personen: 50 EUR
- für 3 Personen: 40 EUR
- ab 4 Personen: 30 EUR
- inkl. 2x BBQ pro Woche im Surfcamp
- inkl. 1x gefüllter Kühlschrank oder Frühstücksbuffett im Surfcamp
- Kinder bis 12 Jahre: 50% Rabatt
- Babys bis 2 Jahre: kostenlos

Hier geht es zur [Buchung]({{ links.de.onlineBooking.path }})
