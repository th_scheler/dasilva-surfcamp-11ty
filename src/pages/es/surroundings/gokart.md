---
tags: surroundings
partial: gokart
webLink: www.dinokart.com.pt
---

### Go Kart

Einfach mal ne Runde drehen? Geht im Dinokart – und natürlich nicht nur eine. Gas geben schön um die Wette fahren statt rennen! Cabriofeeling im Miniformat – näher am Asphalt geht nicht!
