---
tags: surroundings
partial: minigolf
mapLink: https://bit.ly/2Zrgjdb
---

### Minigolf Animar Praia da Areia Branca

Schon lange keine ruhige Kugel mehr geschoben? Kein Ding – einfach direkt Richtung Strand nach Praia da Areia Branca und locker aus der Hüfte die Murmeln versenken. Großer Spaß mit den Kids oder für die großen mit gepflegtem Kaltgetränk als erhöhtem Schwierigkeitsgrad 😉
