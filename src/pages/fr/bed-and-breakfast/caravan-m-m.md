---
tags: bedAndBreakfast
partial: caravan-m-m
---
**Caravane M & M** est une caravane confortable avec de la place pour 2 personnes. Les prix pour cela sont exactement les mêmes que pour nos chambres à plusieurs lits. Pour deux personnes, l'avantage est qu'elles sont seules dans la caravane et qu'elles ne doivent pas payer le supplément pour chambre double. La caravane dispose d'un branchement électrique et les toilettes intégrées ne peuvent pas être utilisées. Bien entendu, les hôtes peuvent utiliser la salle de bains et la cuisine de la maison principale, ainsi que l'ensemble des installations extérieures.

Cliquez ici pour [réservation]({{ links.fr.onlineBooking.path }})
