---
tags: bedAndBreakfast
partial: people
---

## People

Notre surfcamp Portugal est une maison ouverte, c'est-à-dire qu'il se passe toujours quelque chose chez nous. En plus des dîners communs, qui sont compris dans le prix, nous organisons souvent des barbecues spontanés ou des sessions autour d'un feu de camp. Comme nous sommes nous-mêmes à moitié portugais, il arrive que des amis à nous (des locaux) viennent s'asseoir avec nous autour du feu de camp. Il n'est alors pas rare que quelqu'un sorte soudain une guitare et que la soirée se prolonge bien plus longtemps que prévu. Mais il y a aussi des endroits très sympas pour faire la fête dans les environs, où nous vous emmènerons volontiers dans notre navette. Il ne s'agit pas seulement d'apprendre à surfer, mais aussi de découvrir la vie nocturne portugaise.
