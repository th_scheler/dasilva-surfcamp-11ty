---
tags: bedAndBreakfast
partial: quarto-azul
subtitle: Girls-4-Bedroom
---

Notre chambre à 4 lits au rez-de-chaussée est accessible directement depuis la salle de la cheminée. Elle est équipée de deux lits superposés et les hôtes ont leur propre salle de douche à disposition. Juste à côté de la porte, il y a aussi des toilettes pour les invités dans la salle de la cheminée. Les lits simples ne peuvent être réservés que par des femmes. Mais bien sûr, tous les autres sexes sont également autorisés si vous réservez toute la chambre.

Prix par personne, petit-déjeuner et 2 BBQ par semaine inclus :

- Juillet à septembre : 35 EUR
- Avril à juin et octobre : 25 EUR
- Occupation : 1-4 personnes

Cliquez ici pour [Réservation]({{ links.fr.onlineBooking.path }})
