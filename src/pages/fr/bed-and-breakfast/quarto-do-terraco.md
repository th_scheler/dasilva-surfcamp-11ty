---
tags: bedAndBreakfast
partial: quarto-do-terraco
subtitle: Chambre double ou à 4 lits
---

La chambre terrasse se trouve au premier étage de notre maison principale, au-dessus de la salle de la cheminée. La chambre dispose d'un lit superposé avec un matelas double. En dessous, il y a des toilettes avec un lavabo et une petite table où il est agréable de jouer aux cartes. À l'avant de la pièce, il y a un coin salon confortable. Le canapé peut être rabattu pour former un lit double confortable, de sorte que la chambre convient bien à deux couples ou, mieux encore, à un couple avec deux enfants. Mais le point fort est sans aucun doute la grande terrasse ensoleillée (partiellement couverte), d'où l'on a une vue magnifique sur le vaste paysage jusqu'à l'Atlantique. La terrasse est également accessible par un escalier extérieur (depuis la cour intérieure de Mario et Elke). C'est pourquoi d'autres hôtes y prennent parfois le soleil, mais le soir, nous demandons à ce que le sommeil soit préservé. On peut se doucher dans la salle de bain commune au rez-de-chaussée ou dans la douche extérieure (chaude) au coin de la rue.

Prix par personne, petit-déjeuner et 2x BBQ par semaine inclus :

- Juillet à septembre : 45 EUR
- Avril à juin et octobre : 35 EUR
- Occupation minimale : 2 personnes (adultes).
- Enfants jusqu'à 12 ans : 50% de réduction
- Bébés jusqu'à 2 ans gratuits

Cliquez ici pour [Réservation]({{ links.fr.onlineBooking.path }})
