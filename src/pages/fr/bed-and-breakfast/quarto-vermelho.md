---
tags: bedAndBreakfast
partial: quarto-vermelho
subtitle: Mixte / 7 lits
---

Notre plus grande chambre à plusieurs lits se trouve dans un bâtiment séparé, juste au coin de la cuisine et en face de notre bar. Dans la chambre, il y a trois lits superposés et au-dessus de la salle de bain avec douche, il y a un lit superposé (mezzanine) avec un autre matelas simple, idéal pour les personnes de plus de 2 mètres, car le pied de lit est ouvert. Dehors, à côté de la porte d'entrée, il est possible de prendre une douche chaude en plein air tout en profitant de la vue sur le vaste paysage.

Prix par personne, petit-déjeuner et 2x BBQ par semaine inclus :

- Juillet à septembre : 45 EUR
- Avril à juin et octobre : 35 EUR
- 4 personnes ou plus (adultes) : 35 EUR ou 25 EUR
- Occupation minimale : 3 personnes (adultes).
- Enfants jusqu'à 12 ans : 50% de réduction
- Bébés jusqu'à 2 ans gratuits

Cliquez ici pour [Réservation]({{ links.fr.onlineBooking.path }})
