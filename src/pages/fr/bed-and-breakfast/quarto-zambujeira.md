---
tags: bedAndBreakfast
partial: quarto-zambujeira
subtitle: Chambre double
---

La chambre double au premier étage de la maison principale est accessible par l'escalier intérieur depuis la cuisine ou le salon avec cheminée. La chambre dispose de deux lits simples qui peuvent être réunis pour former un lit double. Si nécessaire, il est également possible d'ajouter un lit d'enfant. La fenêtre offre une vue magnifique sur le vaste paysage jusqu'au village de Zambujeira situé en face. La salle de bain avec douche du rez-de-chaussée est à partager avec les hôtes qui habitent les deux autres chambres du premier étage. Dans le salon avec cheminée, il y a cependant encore un WC pour les invités en cas d'urgence.

Prix par personne, petit-déjeuner et 2x BBQ par semaine inclus :

- Juillet à septembre : 40 EUR
- Avril à juin et octobre : 30 EUR
- Occupation minimale : 2 personnes (adultes)
- Enfants jusqu'à 12 ans : 50% de réduction
- Bébés jusqu'à 2 ans gratuits

Cliquez ici pour [Réservation]({{ links.fr.onlineBooking.path }})
