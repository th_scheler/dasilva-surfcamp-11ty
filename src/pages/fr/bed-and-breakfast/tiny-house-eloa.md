---
tags: bedAndBreakfast
partial: tiny-house-eloa
---

La "Tinyhouse Eloá" se trouve un peu plus loin sur la pente en direction de la mini-rampe. Un rêve pour les skaters parmi vous ! Qui n'a jamais rêvé de se réveiller à côté d'une belle mini-rampe ? Pour le reste, la maisonnette offre, comme l'autre, une petite kitchenette, un réfrigérateur, une salle de bain avec WC et douche, et la même vue magnifique sur la vaste campagne jusqu'aux villages d'en face, sur lesquels le soleil se lève le matin et brille directement dans la chambre. Les lève-tard devraient donc tirer les rideaux avant de s'endormir 🙂

Prix par personne, petit-déjeuner et 2x BBQ par semaine inclus :

- Juillet à septembre : 50 EUR
- Avril à juin et octobre : 40 EUR
- Occupation minimale : 2 personnes (adultes)
- Enfants jusqu'à 12 ans : 50% de réduction
- Bébés jusqu'à 2 ans gratuits

Cliquez ici pour [Réservation]({{ links.fr.onlineBooking.path }})
