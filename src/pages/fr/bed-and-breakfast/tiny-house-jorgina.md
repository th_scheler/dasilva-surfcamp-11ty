---
tags: bedAndBreakfast
partial: tiny-house-jorgina
---

À environ 20 mètres du Da Silva Surfcamp se trouve cette maisonnette en bois séparée, idéale pour les personnes qui souhaitent participer à la vie sociale d'un surfcamp, mais qui veulent pouvoir s'isoler à tout moment. La maisonnette a un climat intérieur très agréable. Pendant la journée, il ne fait pas trop chaud et la nuit, la chaleur agréable du soleil se maintient dans la maison. Si l'on veut cuisiner quelque chose, la cuisine commune du surfcamp est à disposition. En chemin, on passe directement devant notre bar, où l'on peut se rafraîchir avant ou après l'épuisant travail de cuisine.

Prix par personne, petit-déjeuner et 2x BBQ par semaine inclus :

- Juillet à septembre : 50 EUR
- Avril à juin et octobre : 40 EUR
- Occupation minimale : 2 personnes (adultes)
- Enfants jusqu'à 12 ans : 50% de réduction
- Bébés jusqu'à 2 ans gratuits

Cliquez ici pour [Réservation]({{ links.fr.onlineBooking.path }})
