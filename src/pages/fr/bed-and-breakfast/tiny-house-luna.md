---
tags: bedAndBreakfast
partial: tiny-house-luna
---

Depuis le printemps 2019, nous sommes heureux de pouvoir vous proposer notre Tiny House Luna - avec tout ce dont un homme & une femme ont besoin. Une petite kitchenette, un réfrigérateur, une salle de bain avec WC et douche, et surtout, un emplacement calme avec une vue magnifique. Enjoy !

Le matelas du lit superposé mesure 140 x 190 cm. Le canapé-lit en bas mesure 128 x 180 cm. Mais si on le déplie, il n'y a presque plus de place dans la maisonnette.

Prix par personne, petit-déjeuner et 2x BBQ par semaine inclus :

- Juillet à septembre : 50 EUR
- Avril à juin et octobre : 40 EUR
- Occupation minimale : 2 personnes (adultes)
- Enfants jusqu'à 12 ans : 50% de réduction
- Bébés jusqu'à 2 ans gratuits

Cliquez ici pour [Réservation]({{ links.fr.onlineBooking.path }})
