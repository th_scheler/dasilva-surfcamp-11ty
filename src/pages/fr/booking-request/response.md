---
permalink: /fr/booking-request/response/index.html
layout: confirmation.njk
tags: bookingRequestResponse
partial: response
locale: fr
---

## Demande de réservation reçue

Merci pour la demande de réservation. Nous leur répondrons dans les plus brefs délais.

Cordialement<br>
Daniel W Da Silva
