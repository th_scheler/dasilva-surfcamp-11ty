---
tags: camping
partial: camping
headline: No Glam / Just Camping!
subtitle: ""
---

Pour ceux qui aiment voyager économe, nous avons l'option parfaite : le camping ! Il est particulièrement peu coûteux si vous apportez votre propre tente. Alternativement, nous proposons également des emplacements avec électricité pour mobil-homes et caravanes.

Notre toute nouvelle cuisine extérieure et une salle de bain moderne seront à votre disposition à partir de 2023. Nous avons construit ces installations spécialement pour vous, afin que vous puissiez rendre votre séjour chez nous encore plus agréable et confortable. Profitez du sentiment de liberté et d'indépendance en camping sans avoir à renoncer au confort d'une cuisine et d'une salle de bain modernes et soyez au milieu de la vie de surf camp.


* Nuitée dans votre propre tente, mobil-home ou caravane : 10 EUR par personne et par nuit (les enfants jusqu'à 2 ans sont gratuits et les enfants jusqu'à 12 ans paient la moitié)
* Forfait emplacement pour mobil-homes ou caravanes : 10 EUR par nuit électricité comprise
* Tous les prix incluent l'utilisation partagée de la salle de bain et de la cuisine extérieure
* NON inclus dans le prix sont le petit déjeuner buffet quotidien (7 EUR) et notre barbecue (13 EUR, dimanche et vendredi). Vous êtes invités à le réserver en tant que service supplémentaire et à rendre votre expérience de camping encore plus inoubliable.


La prochaine station d'élimination n'est qu'à un jet de pierre de nous. Il est situé au supermarché Intermarché de Lourinhã et est accessible en seulement 5 minutes. Nous vous offrons également la possibilité de remplir votre réservoir d'eau douce avec nous.
