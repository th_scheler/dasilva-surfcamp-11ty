---
tags: contact
partial: form
locale: fr

name:
  label: Nom de famille
email:
  label: E-Mail
comment:
  label: Commentaire ou message
submit:
  label: Soumettre
---

## Contact

{% include "contact-form.njk" %}
