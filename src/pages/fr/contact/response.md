---
permalink: /fr/contact/response/index.html
layout: confirmation.njk
tags: contactResponse
partial: response
locale: fr
---

## Message reçu

Merci de nous contacter. Nous leur répondrons dans les plus brefs délais.

Hang Loose uet salutations de Praia da Areia Branca<br>
Da Silva Surfcamp Team
