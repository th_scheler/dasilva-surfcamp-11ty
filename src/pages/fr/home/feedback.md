---
tags: home
partial: feedback
---

### Et voici ce qu'écrivent nos hôtes :

"Nous avons passé un super moment au Da Silva Surfcamp - le terrain décontracté, aménagé avec beaucoup de charme et de créativité, offre l'espace idéal pour le repos de toute la famille : Sessions de surf sur les nombreux spots des environs, journées à la plage, chill-out au camp, yoga, balades à poney, skate, feux de camp sous les étoiles, soirées conviviales au bar avec des tournois de baby-foot, petit-déjeuner avec des options sans gluten et végétaliennes, délicieuses soirées BBQ et des gens tout simplement détendus - nous repartons avec d'innombrables beaux souvenirs dans nos bagages. Mais ce qui a surtout marqué cette période pour nous, c'est l'équipe cinq étoiles de tout premier ordre 2022, grâce à laquelle le Da Silva Surfcamp est devenu pour nous un happy place absolu - merci à tous de nous avoir gâtés - nous avons prolongé (pas seulement une fois :-) et nous reviendrons avec plaisir !"
