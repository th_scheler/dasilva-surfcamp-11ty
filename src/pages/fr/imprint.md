---
permalink: /fr/imprimer/index.html
layout: plain-text.njk
tags: imprint
locale: fr
navigation: Imprimer

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Imprimer"

# max 158 characters
metaDescription: "Impremier et Adresse"
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/surf-mtb.jpg"
---

# Imprimer

### Propriétaire/éditeur

```
Daniel Wohlang da Silva
Da Silva Surfcamp Portugal
Casal da Capela, Casal da Murta
Rua Pôr do Sol, N° 21
2530 – 077 Lourinhã - Portugal
Téléphone mobile: +351 913 818 750
(Appel / WhatsApp / Signal)
Téléphone (ligne fixe): +351 / 261 461 515
E-Mail: surfcamp[@]dasilva.de
```

### Droits d'auteur

Le contenu et les œuvres créés par l'exploitant du site sont protégés par le droit d'auteur. Cela s'applique à la fois au design et au contenu. La reproduction, le traitement ou les modifications ne sont autorisés qu'après accord écrit. De même, tous les droits de reproduction, sous quelque forme et sur quelque support que ce soit, sont réservés.

### Clause de non-responsabilité

Contenu propre | Toutes les informations sont régulièrement et soigneusement mises à jour au mieux de nos connaissances et de nos convictions. Au sens juridique, cependant, ils ne sont pas garantis.
Contenu étranger | À certains endroits, nous renvoyons à des sites externes (liens). Nous n'avons aucune influence sur leur conception, leur contenu, leur actualité, leur exhaustivité et leur exactitude. Nous nous dissocions donc expressément de tous les contenus de ces pages, ainsi que de toutes les pages auxquelles mènent les pages liées par nous.

# Licences

### Police géniale

Nous utilisons des icônes sociales et d'autres icônes ludiques de **Font Awesome** avec la licence **Creative Commons Attribution 4.0 International** et exige que nous liions à <a href="https://fontawesome.com/license" target="_blank">Licences Font Awesome</a>. Merci pour les merveilleuses icônes. Nous apprécions à peu près.