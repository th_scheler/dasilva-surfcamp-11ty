---
tags: kidsAreWelcome
partial: discount
---

En termes de prix, nous essayons d'accommoder les parents en ne facturant rien pour les bébés jusqu'à deux ans. Nous fournissons gratuitement des lits bébé et pour les enfants jusqu'à 12 ans qui dorment avec leurs parents sur un lit supplémentaire dans la chambre, nous ne facturons que la moitié. Nous proposons des cours de surf pour les enfants à partir de 6 ans, mais sans réduction, car un moniteur de surf supplémentaire doit être engagé. Les parents et les enfants sont toujours dans un cours ensemble. Pour les parents avec de jeunes enfants, nous leur proposons de partager un cours de surf afin que l'un d'eux puisse toujours s'occuper de l'enfant. Alternativement, nous pouvons organiser une baby-sitter.