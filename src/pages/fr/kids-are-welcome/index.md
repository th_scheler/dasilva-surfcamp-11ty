---
permalink: /fr/kids-are-welcome/index.html
layout: kids-are-welcome.njk
tags: kidsAreWelcome
locale: fr
navigation: Kids Welcome

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Les enfants sont les bienvenus"

# max 158 characters
metaDescription: "Du plaisir pour toute la famille | Surfer sur la plage avec papa et maman ou en mer | Monter à cheval dans le camp | beaucoup d'autres enfants | familiy"
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/kids-are-welcome.jpg"
---
