---
permalink: /fr/mountain-bike/index.html
layout: mountain-bike.njk
tags: mountainBike
locale: fr
navigation: Mountain Bike

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Mountain Bike Tours » MTB Joyride"

# max 158 characters
metaDescription: "Circuits VTT à travers de superbes paysages, des queues fraîches et le long de la côte | Roues haut de gamme | Surf & Mountainbike Package"
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/mountain-bike.jpg"
---
