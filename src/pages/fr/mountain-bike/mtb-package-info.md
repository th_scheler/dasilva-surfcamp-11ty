---
tags: mountainBike
partial: mtb-package-info
---

<div class="h3">Quelques informations supplémentaires :</div>

Si vous voulez rouler avec Klickies, vous devez apporter vos propres pédales et chaussures. Nous vous recommandons également d'apporter votre propre casque, des gants de vélo et un sac à dos pour les provisions (eau, fruits et quelques barres de muesli). Un dos de chameau (sac à dos d'eau potable) serait le mieux. Des pantalons de cyclisme (rembourrés) et des sous-vêtements de sport généralement fonctionnels (au moins 2x complets) doivent également être emportés avec vous. Nos guides ont toujours avec eux des kits de réparation, des démonte-pneus, des pompes, des chambres à air de rechange, une patte de dérailleur et une trousse de premiers secours lors de leurs visites.
