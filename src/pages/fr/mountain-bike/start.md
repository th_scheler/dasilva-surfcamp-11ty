---
tags: mountainBike
partial: start
---

## DA SILVA BIKECAMP Portugal

<h3 style="margin-top: -1rem;">Découvrez la côte atlantique portugaise<br>avec le</h3>

<h1 style="margin : -2.1rem 0 2rem 0 ;">VTT</h1>

La côte ouest du Portugal est sans aucun doute un paradis du surf. Des plages fantastiques et des spots de surf de première classe attirent de nombreux surfeurs du monde entier. Cependant, beaucoup ne savent pas encore que notre côte est aussi idéale pour le VTT !

## À l'assaut du Portugal en VTT

La zone autour de notre camp offre un immense terrain de jeu avec d'innombrables superbes sentiers de vélo de montagne. Il y a des visites à travers l'arrière-pays rustique en passant par les vignobles et les oliveraies, où vous rencontrez rarement d'autres personnes. Il est plus probable de passer devant une charrette à âne ou de voir au loin un fermier solitaire travailler dans son champ. D'autres circuits mènent le long de sentiers étroits directement le long des falaises et offrent une vue imprenable sur l'Atlantique. De nombreux spots de surf secrets ont déjà été découverts de cette manière. Avec notre navette, nous vous conduirons également dans des montagnes beaucoup plus hautes, parfaites pour des excursions d'une journée inoubliables. Vous y trouverez également des itinéraires de descente développés et un tas de sentiers simples difficiles pour tous les goûts. Dans tous les cas, vous apprenez à connaître le Portugal d'une manière particulière lors de toutes les visites. Nous aimerions vous rapprocher du pays, des gens et de leur culture. Les exigences sportives ne sont pas négligées. Il y a toujours de l'aventure et de l'adrénaline.

