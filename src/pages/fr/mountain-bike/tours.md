---
tags: mountainBike
partial: tours
---

## Nos circuits

Nous parcourons généralement 40-60 km et arrivons à 800-1200 mètres d'altitude. La plupart du temps, c'est plus du cross-country, mais il y a toujours de l'enduro et parfois même un peu de descente. Cela dépend avant tout des souhaits et des exigences de nos participants, auxquels nous sommes heureux de nous conformer. Mais il devrait toujours y avoir quelque chose pour tout le monde. Les visites suivantes ne sont que des exemples. Il en existe d'innombrables variantes, que vous pouvez voir ici sur [![Strava](/_assets/mtb-tours/strava.jpg)](https://www.strava.com/athletes/17635626).
