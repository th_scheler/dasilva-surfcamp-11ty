---
tags: mtbTourBarragemSaoDomingos
partial: data
---

- Parcours 34 km
- Temps de parcours pur 2:31 h
- Dénivelé 455 m
- <a href="https://strava.app.link/LCL8AVXoNZ" target="_blank">![Strava link](/_assets/mtb-tours/strava.jpg)</a>