---
tags: mtbTourBarragemSaoDomingos
partial: start
---

# Barragem de São Domingos

An der Küste entlang nach Consoloção bis zum Stausee von Atouguia da Baleia. Eine abwechslungs-reiche Hausrunde mit verschiedenen technischen Passagen und spannender Szenerie. Gesamte Dauer mit Pausen: ca. 3,5 Stunden Schwierigkeit: S0, wenige Passagen bis S1.
