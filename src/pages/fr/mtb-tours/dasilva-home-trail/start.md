---
tags: mtbTourDasilvaHomeTrail
partial: start
---

# Da Silva Bikecamp<br>Hometrail

Abwechslungsreiche Hausrunde durch die Hügel nordöstlich von Lourinhã und zum Strand in Praia Areia Branca, mit verschiedenen technischen Passagen und spannender Szenerie. Gesamte Dauer mit Pausen: ca. 5 Stunden Schwierigkeit: S0, mit Passagen bis S1.
