---
tags: mtbTourSantaCruz
partial: data
---

- Strecke 34 km
- Reine Fahrzeit 3:22 h
- Höhenmeter 702 m 
- [![Strava link](/_assets/mtb-tours/strava.jpg)](https://strava.app.link/kQdmMHOCHZ)
