---
permalink: /fr/mtb-tours/santa-cruz/index.html
layout: mtb-tours-santa-cruz.njk
tags: mtbTourSantaCruz
locale: fr
navigation: Santa Cruz

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Mountain Bike Tours » Santa Cruz"

# max 158 characters
metaDescription: "MTB Tour entlang der Steilküste Portugal mit dem Blick auf das Meer | Praia Areia Branca | Ribamar | Vimeiro | Santa Rita | Santa Cruz"

# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/santa-cruz.jpg"
---
