---
tags: prices
partial: bedAndBreakfastPrices
---

- Basse saison
  - (Oct. – Juin)
  - Multiples : 25 €
  - Double : 30 €
  - Tiny House : 35 €
  - Simple : 55 €
- Haute saison
  - (Juillet – Sep.)
  - Multiples : 35 €
  - Double : 40 €
  - Tiny House : 50 €
  - Simple : 65 €

<div style="text-align: center">
Location de <a href="https://www.dasilva-surfcamp.de/fr/e-bike/" target="_blank">vélos électriques</a> CUBE de haute qualité à partir de 25 € par jour<br>
</div>