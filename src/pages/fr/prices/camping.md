---
tags: prices
partial: camping
---

## No Glam / Just Camping!

Pour les renards frugaux, nous avons aussi la possibilité de camper. Il est préférable que vous apportiez votre propre tente, mais vous pouvez également louer des tentes chez nous. Nous proposons également des emplacements avec électricité pour mobil-homes ou caravanes.


* Nuitée dans votre propre tente, mobil-home ou caravane : 10€ par personne/nuit
* Forfait emplacement mobil-home ou caravane : 10€ par nuit (électricité comprise)
* Location tente : 10€ par tente/nuit (pour 1-2 personnes)
* Tous les prix incluent l'utilisation partagée de la salle de bain et de la cuisine partagées.
* Le petit-déjeuner buffet (7 EUR) et le barbecue (13 EUR) ne sont PAS inclus dans le prix.


La prochaine "dumping point" est à seulement 4 km de chez nous au supermarché Intermarché de Lourinhã.
