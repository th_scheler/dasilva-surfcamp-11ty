---
permalink: /fr/prix/index.html
layout: prices.njk
tags: prices
locale: fr
navigation: Prix

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Des Prix"

# max 158 characters
metaDescription: "Hébergement à partir de 25€ | Forfait Surf à partir de 395€ la semaine | Surf & VTT à partir de 505€ la semaine | Ecole de Surf à partir de 60€ la journée"
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/surf-mtb.jpg"
---
