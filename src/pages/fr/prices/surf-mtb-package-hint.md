---
tags: prices
partial: surf-mtb-package-hint
---

<div class="h4">Inclus dans le prix</div>

VTT tout suspendu CUBE de haute qualité (roues 29 pouces). Les casques sont fournis.
**Veuillez apporter votre propre sac à dos avec de l'eau, des fruits et quelques barres de muesli.**
