---
tags: prices
partial: surf-mtb-package-including
---

<div class="h4">y compris</div>

* 7 jours en chambre d'hôtes au camp Da Silva
* 2x barbecue (également végétarien) avec feu de camp
* 3 jours de cours de surf (2 heures par session / 2 sessions par jour) équipement de surf complet (planche de surf, leash, combinaison)
* Navette pour les plages avec les meilleures vagues de notre région
* 3 circuits VTT (4-6 heures / 40-60 km par jour)
* Vélo de montagne CUBE de haut niveau (29 pouces) avec casque
* Navette vers les meilleurs sentiers VTT de notre région
  