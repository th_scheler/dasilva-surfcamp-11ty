---
tags: prices
partial: surf-mtb-package-prices
---

- Basse saison
  - (Oct. – Juin)
  - Multiples : 505 €
  - Double : 540 €
  - Tiny house : 575 €
  - Simple : 715 €
- Haute saison
  - (Juillet – Sep.)
  - Multiples : 605 €
  - Double : 640 €
  - Tiny house : 710 €
  - Simple : 815 €
