---
tags: prices
partial: surfPackage2WeeksPrices
---

- Basse saison
  - (Oct. – Juin)
  - Multiples :  euros
  - Double : 820 €.
  - Petite maison : 890 €.
  - Simple : 1170 €.
- Haute saison
  - (Juillet – Sep.)
  - Multiples : 840 euros
  - Double : 910
  - Tiny House : 980
  - Simple : 1330 €.
