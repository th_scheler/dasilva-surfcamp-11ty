---
tags: prices
partial: surfPackagePrices
---

- Basse saison
  - (Oct. – Juin)
  - Multiples : 395 €
  - Double : 430 €
  - Tiny house : 465 €
  - Simple : 605 €
- Haute saison
  - (Juillet – Sep.)
  - Multiples : 450 €
  - Double : 485 €
  - Tiny house : 520 €
  - Simple : 695 €
