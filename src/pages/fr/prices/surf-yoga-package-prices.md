---
tags: prices
partial: surfYogaPackagePrices
---

- Basse saison
  - (Oct. – Juin)
  - Multiples : 455 €
  - Double : 490 €
  - Tiny House : 525 €
  - Simple : 665 €
- Haute saison
  - (Juillet – Sep.)
  - Multiples : 510 €
  - Double : 545 €
  - Tiny House : 580 €
  - Simple : 755 €
