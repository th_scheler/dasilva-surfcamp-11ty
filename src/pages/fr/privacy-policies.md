---
permalink: /fr/declaration-confidentialite/index.html
layout: plain-text.njk
tags: privacyPolicies
locale: fr
navigation: Protection des données

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Protection des données"

# max 158 characters
metaDescription: "Protection des données | Privacy Policies"
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/surf-mtb.jpg"
---

# Protection des données/ declaration de confidentialite

Notre site Web peut généralement être utilisé sans fournir de données personnelles. Dans la mesure où des données personnelles (par exemple, nom, adresse ou adresses e-mail) sont collectées sur notre site Web, cela se fait toujours sur une base volontaire dans la mesure du possible. Ces données ne seront pas transmises à des tiers sans votre consentement exprès. Nous attirons votre attention sur le fait que la transmission de données sur Internet (par exemple lors de la communication par e-mail) peut présenter des failles de sécurité. Une protection complète des données contre l'accès par des tiers n'est pas possible. L'utilisation des données de contact publiées dans le cadre de l'obligation d'impression par des tiers pour envoyer de la publicité et du matériel d'information non sollicités est expressément interdite. Les exploitants du site se réservent expressément le droit d'engager des poursuites en cas d'envoi de publicités non sollicitées, telles que des spams.


### Déclaration de protection des données pour l'utilisation de Google Analytics

Ce site Web utilise Google Analytics, un service d'analyse Web fourni par Google Inc. (« Google »). Google Analytics utilise des "cookies", des fichiers texte qui sont stockés sur votre ordinateur et permettent une analyse de votre utilisation du site Web. Les informations générées par le cookie concernant votre utilisation de ce site Web (y compris votre adresse IP) sont transmises à un serveur de Google aux États-Unis et y sont stockées.

Google utilisera ces informations pour évaluer votre utilisation du site Web, pour compiler des rapports sur l'activité du site Web pour les opérateurs de site Web et pour fournir d'autres services liés à l'activité du site Web et à l'utilisation d'Internet. Google peut également transférer ces informations à des tiers si la loi l'exige ou si des tiers traitent ces données pour le compte de Google. Google n'associera en aucun cas votre adresse IP à d'autres données détenues par Google Inc.

Vous pouvez empêcher le stockage de cookies en configurant votre logiciel de navigation en conséquence ; nous attirons toutefois votre attention sur le fait que dans ce cas, vous ne pourrez éventuellement pas utiliser toutes les fonctions de ce site Internet dans leur intégralité. Vous pouvez également empêcher Google de collecter les données générées par le cookie et liées à votre utilisation du site Web (y compris votre adresse IP) et de traiter ces données par Google en téléchargeant le plug-in de navigateur disponible sous le lien suivant et en l'installant : https ://tools.google.com/dlpage/gaoptout?hl=de.

### Déclaration de protection des données pour l'utilisation de Twitter

Les fonctions du service Twitter sont intégrées sur nos sites. Ces fonctions sont proposées par Twitter Inc., 795 Folsom St., Suite 600, San Francisco, CA 94107, USA. En utilisant Twitter et la fonction "Re-Tweet", les sites Web que vous visitez sont liés à votre compte Twitter et portés à la connaissance des autres utilisateurs. Ces données sont également transmises à Twitter.

Nous attirons votre attention sur le fait que nous, en tant que fournisseur des pages, n'avons aucune connaissance du contenu des données transmises ni de la manière dont elles sont utilisées par Twitter. Pour plus d'informations, consultez la politique de confidentialité de Twitter à l'adresse https://twitter.com/privacy.

Vous pouvez modifier vos paramètres de confidentialité sur Twitter dans les paramètres du compte à l'adresse https://twitter.com/account/settings.
