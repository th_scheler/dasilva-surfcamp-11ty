---
tags: skateboarding
partial: miniRamp
---

Our Homespot: a 5m „Miniramp“ directly behind Bar! – check it out!

<a href="https://g.co/maps/9xh8n" target="_blank"><img src="/_assets/icons/svgrepo/google-maps.svg" class="surroundings-cards__map-link" alt="google maps link"></a>