---
tags: skateboarding
partial: skatePark
---

Si vous voulez avoir le programme complet juste au coin de la rue, le skatepark Lourinhã est l'endroit idéal pour les skateurs - à seulement 3 km au sud de Praia da Areia Branca, de douces rampes, des escaliers, des rampes, des bancs et des pipes fines vous attendent.

<a href="https://bit.ly/2TOFqXt" target="_blank"><img src="/_assets/icons/svgrepo/google-maps.svg" class="surroundings-cards__map-link" alt="google maps link"></a>
