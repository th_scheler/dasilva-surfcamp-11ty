---
permalink: /fr/yoga/index.html
layout: yoga.njk
tags: yoga
locale: fr
navigation: Yoga

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Surf e Yoga"

# max 158 characters
metaDescription: "Yoga pour le bien-être physique, émotionnel et spirituel | pratique traditionnelle du hatha yoga | Surf e Yoga Package"
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/yoga.jpg"
---
