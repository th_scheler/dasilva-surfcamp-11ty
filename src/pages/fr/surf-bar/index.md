---
permalink: /fr/surf-bar/index.html
layout: surf-bar.njk
tags: surfBar
locale: fr
navigation: Surf Bar

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Da Silva Surf Bar"

# max 158 characters
metaDescription: "Boire, faire la fête, sortir le soir"
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/surf-bar.jpg"
---
