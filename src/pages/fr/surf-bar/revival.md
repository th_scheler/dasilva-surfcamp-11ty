---
tags: surfBar
partial: revival
---

## Da Silva Surfcamp REVIVAL

Et parce que c'était et c'est tellement agréable et qu'il faut sortir de la maison de temps en temps, il y a une réunion une fois par an - toujours quand il fait le plus froid à Berlin. Pour ceux qui y étaient et tous ceux qui voudraient y aller.

#### C U THERE & HANGLOOSE
