---
tags: surfBar
partial: surf-bar
---

Bien sûr, nous aimons toujours faire la fête. Et bien sûr on a toujours envie de sortir de temps en temps. À l'extérieur dans tous les cafés de plage, bars de surf et barracas ou au milieu de la vie nocturne de Praia da Areia Branca ou Baleal. Mais les très grandes fêtes, les anniversaires marquants, les mariages ou le 100 000e visiteur, nous les célébrons désormais à la maison. Et comme c'est plus facile à faire dans un bar qu'ailleurs, nous nous en sommes tout simplement construit un. Le SURF BAR DA SILVA. Et puisque nous en avons, eh bien, parfois nous célébrons aussi les petites fêtes...
