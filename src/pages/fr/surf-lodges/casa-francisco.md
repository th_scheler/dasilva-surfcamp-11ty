---
tags: surfLodges
partial: casa-francisco
---

Casa Francisco est située dans la charmante ville de Casal da Murta, de l'autre côté de la route de campagne que vous devez traverser pour vous rendre à notre camp de surf. Situé dans une rue calme, des chemins de terre mènent à de belles baies de baignade isolées (à seulement 1 km). Des meubles de jardin et un espace barbecue sont disponibles. Sur le toit il y a une grande terrasse avec une belle vue sur la campagne. L'appartement dispose de deux chambres (un lit double et deux lits simples), salon avec cheminée, TV et accès internet, cuisine et salle de bain. Les propriétaires vivent dans la maison principale et sont très sympathiques et serviables. La cour est partagée avec les hôtes du "Studio Prata" et du "Quarto Por do Sol". Les trois logements réunis peuvent accueillir 8 personnes.

<b>Prix par personne avec petit déjeuner* et 2x BBQ par semaine :</b>

- pour 2 personnes : 50 euros
- pour 3 personnes : 40 euros
- à partir de 4 personnes : 30 EUR
- y compris 2x BBQ par semaine dans le surf camp
- y compris 1x réfrigérateur rempli ou buffet de petit-déjeuner dans le camp de surf
- Enfants jusqu'à 12 ans : 50 % de réduction
- Bébés jusqu'à 2 ans : gratuit

Cliquez ici pour [réservation]({{ links.fr.onlineBooking.path }})
