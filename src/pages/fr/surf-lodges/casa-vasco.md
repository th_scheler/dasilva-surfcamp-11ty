---
tags: surfLodges
partial: casa-vasco
---

Goreti Vasco vit en Allemagne avec sa famille depuis de nombreuses années. Maintenant, elle a réalisé son rêve de posséder une maison au Portugal et loue deux appartements de vacances à 400 m de notre surf camp. Elle se réjouit des invités avec qui elle peut converser en allemand. Si vous vous promenez tranquillement le long des routes de campagne à partir d'ici, vous pourrez rejoindre en peu de temps les plages de baignade voisines. Vous pourrez profiter de la cour intérieure et d'un petit pré avec vue sur la campagne. L'appartement se compose d'un grand séjour-cuisine avec cheminée et TV, de deux chambres doubles (chacune 135 x 185 cm) et d'une salle de bain avec baignoire. Il y a aussi une deuxième cuisine avec une cheminée où vous pourrez faire un bon barbecue.

<b>Prix par nuit avec petit-déjeuner* et 2x BBQ par semaine au surf camp:</b>

- pour 2 personnes : 110 EUR
- pour 3 personnes : 135 EUR
- pour 4 personnes : 160 EUR
- Occupation minimum : 2 personnes (adultes)
- Bébés jusqu'à 2 ans : gratuit

Cliquez ici pour [réservation]({{ links.de.onlineBooking.path }})
