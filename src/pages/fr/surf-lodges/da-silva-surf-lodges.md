---
tags: surfLodges
partial: da-silva-surf-lodges
---

Nous proposons les "Da Silva Surf Lodges" comme alternative exclusive à notre camp de surf. A seulement 300 mètres de nous, vous pourrez profiter de vos vacances dans une ambiance idyllique. Cinq appartements sont disponibles avec une chambre, un salon spacieux avec une cuisine ouverte et une salle de bain confortable. Le sixième appartement dispose de deux chambres et de deux salles de bains (prix sur demande). Smart TV, W-Lan, lave-vaisselle sont une évidence. Devant le bâtiment, il y a un petit lac avec une source naturelle. Le gazebo en bois sur le lac est idéal pour lire un bon livre tout en écoutant le gazouillis des oiseaux :)

**Prix par personne et par nuit avec petit déjeuner et 2x BBQ par semaine :**

**juillet à septembre**

- pour 2 personnes : 60 euros
- pour 3 personnes : 50 euros
- à partir de 4 personnes : 40 EUR

**Octobre à juin**

- pour 2 personnes : 50 euros
- pour 3 personnes : 40 euros
- à partir de 4 personnes : 35 EUR

**Les conditions**

- Occupation minimum : 2 personnes (adultes)
- Bébés jusqu'à 2 ans : gratuit

Cliquez ici pour [réservation]({{ links.fr.onlineBooking.path }})
