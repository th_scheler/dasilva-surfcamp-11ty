---
tags: surfLodges
partial: studio-prata
---
Les clients du Studio Prata partagent la cour avec la Casa Francisco. L'appartement petit mais très confortable offre un espace pour deux personnes. Un lit double, une kitchenette, une table à manger, une télévision connectée et une salle de douche moderne ne devraient rien laisser à désirer. Des meubles de jardin et un espace barbecue sont disponibles. Sur le toit il y a une grande terrasse avec une belle vue sur la campagne. Les propriétaires vivent dans la maison principale et sont très sympathiques et serviables. De là, il n'y a que 400 m jusqu'à notre camp de surf. Un chemin de terre mène à de belles baies de baignade désertes même en haute saison.

<b>Prix par personne avec petit déjeuner* et 2x BBQ par semaine :</b>

- pour 2 personnes : 40 euros
- y compris 2x BBQ par semaine dans le surf camp
- y compris 1x réfrigérateur rempli ou buffet de petit-déjeuner dans le camp de surf
- Enfants jusqu'à 12 ans : 50 % de réduction
- Bébés jusqu'à 2 ans : gratuit

Cliquez ici pour [réservation]({{ links.de.onlineBooking.path }})
