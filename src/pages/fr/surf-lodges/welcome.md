---
tags: surfLodges
partial: welcome
---

# Da Silva Surf Lodges

## Maisons de vacances, appartements et studios

### pour des vacances de surf individuelles au Portugal
<br>
Dans les environs de notre camp de surf, il y a plusieurs maisons de vacances, appartements et studios idéaux pour des vacances de surf. L'emplacement calme, un salon confortable avec terrasse et la cuisine bien équipée, dans laquelle vous pouvez vous restaurer, permettent ici des vacances de surf individuelles. Vous n'avez pas à vous passer des commodités d'un camp de surf.

Toutes nos offres sportives, comme les cours de surf, le VTT, le poney, le yoga et le trail running peuvent être réservées. En dehors de cela, vous pouvez venir nous rejoindre à tout moment. Notre Barstuff est heureux de chaque visite et les enfants peuvent sauter dans la piscine pendant que les parents se rafraîchissent au bar. Il n'est pas rare que les conversations entamées là se terminent autour d'un feu de camp romantique. Le prix comprend deux barbecues par semaine et un réfrigérateur rempli pour le petit-déjeuner quotidien ou, si vous le souhaitez, le petit-déjeuner buffet dans le camp de surf.
