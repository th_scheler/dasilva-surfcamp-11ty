---
permalink: /fr/surfschool/index.html
layout: surfschool.njk
tags: surfschool
locale: fr
navigation: Surf School

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Da Silva Surf School - école de surf"

# max 158 characters
metaDescription: "Avec nous, vous pouvez apprendre à surfer facilement et en toute sécurité | Le surf est amusant! | Débutants et avancés | Tarifs et Forfaits"
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/surfschool.jpg"
---
