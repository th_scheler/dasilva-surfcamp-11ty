---
tags: surfschool
partial: low-season
---

### Basse saison

#### *(Novembre – Mars)*

#### ~

#### 1 journée / 2 séances : 60 €

#### 5 jours / 10 séances : 200 €

#### 10 jours / 20 séances : 380 €
