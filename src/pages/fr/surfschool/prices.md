---
tags: surfschool
partial: prices
---

## Tarifs des cours de surf

### Le prix comprend la navette en bus, la planche de surf et la combinaison, les cours théoriques et pratiques et l'assurance.
