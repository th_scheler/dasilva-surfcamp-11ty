---
tags: surroundings
partial: day-trips
---

## Excursions d'une journée

En parlant de ça, non seulement quand c'est plat, nous proposons quelques petites choses. Et avec Porto das Barcas, le Forte de Paimogo, Praia de Valmitão ou encore Consolação - les environs de Lourinha et Praia da Areia Branca viennent immédiatement à l'esprit. Pour tout le reste, comme une excursion en bateau sur les Berlengas, le patrimoine mondial du surf autour d'Ericeira ou la ville médiévale d'Óbidos, nous sommes heureux de prendre une journée de congé...
