---
tags: surroundings
partial: obidos
mapLink: https://goo.gl/maps/sqdvM1BTgng34ARv5
---

Une ville médiévale (25 km au nord) avec d'immenses remparts à l'intérieur desquels aucune voiture n'est autorisée à circuler. En août il y a une grande fête médiévale.
