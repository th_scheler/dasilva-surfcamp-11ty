---
tags: surroundings
partial: peniche
mapLink: https://goo.gl/maps/MCEuLzm2VbhXNXXW9
---

Une authentique ville de pêcheurs (12 km au nord) avec un vieux château sur le port et de nombreux restaurants de poissons. L'archipel "Berlengas" est accessible en ferry en une heure.
