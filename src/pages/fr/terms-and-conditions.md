---
permalink: /fr/conditions-utilisation/index.html
layout: plain-text.njk
tags: termsAndConditions
locale: fr
navigation: Conditions d'utilisation

# "Da Silva Surfcamp Portugal » " = max 35 characters
title: "Conditions d'utilisation"

# max 158 characters
metaDescription: "Conditions d'utilisation"
# relative path. example: "/_assets/surfschool/hero-surfschool.jpg"
metaImage: "/_assets/teasers/surf-mtb.jpg"
---


## Conditions d'utilisation

#### (à partir de novembre 2019)

### 1. Réservation et acompte
Si vous souhaitez réserver avec nous, nous serons heureux de vous envoyer une confirmation de réservation ferme. La meilleure façon de le faire est de remplir notre formulaire de réservation. Avec l'envoi de la confirmation de réservation, nous vous proposons la conclusion d'un contrat de voyage et un acompte de 100 EUR par personne (pour les réservations jusqu'à 10 jours) ou 200 EUR (pour les réservations de 11 jours ou plus) est dû dans un délai d'un semaine à nous payer. Le contrat de voyage est conclu lorsque nous avons reçu l'acompte. Si la réservation est effectuée moins de quatre semaines avant le début du voyage, le montant total est exigible immédiatement. Si nous ne sommes pas en mesure d'enregistrer une réception de paiement dans le délai imparti, nous sommes en droit de résilier le contrat après un rappel avec un délai. Après une résiliation du contrat, nous sommes en droit d'exiger une indemnité forfaitaire conformément à la section 4 de nos conditions générales. Des frais de rappel de 10,00 € sont dus pour chaque rappel.

### 2ème paiement final
Le solde est à régler au plus tard quatre semaines avant le départ. Après réception du paiement, vous recevrez la confirmation de réservation finale et les informations détaillées sur l'arrivée de notre part. Si nous ne sommes pas en mesure d'enregistrer la réception du paiement du montant restant dans le délai imparti, nous sommes en droit de fixer un délai de paiement raisonnable. Si nous ne sommes pas en mesure d'enregistrer une réception de paiement après cette période, nous sommes en droit de résilier le contrat. Après une résiliation du contrat, nous sommes en droit d'exiger une indemnité forfaitaire conformément à la section 4 de nos conditions générales.

### 3. Annulation du voyage par le client

Avant le début du voyage, le client peut se retirer du voyage à tout moment. En cas de désistement, nous pouvons exiger une indemnité forfaitaire, qui est calculée selon les pourcentages suivants :

```
jusqu'à 61 jours avant le départ : 15% du prix total
60 à 45 jours avant le départ : 25% du prix total
44 à 35 jours avant le départ : 50% du prix total
34 à 15 jours avant le départ : 60% du prix total
14 à 7 jours avant le départ : 70% du prix total
à partir de 6 jours avant le départ 80% du prix total

```

Le fournisseur de voyages est libre de réclamer des dommages-intérêts plus élevés et spécifiques. Le client a la possibilité de prouver qu'aucun dommage ou un dommage inférieur au montant forfaitaire réclamé n'a été subi.
### 4. Frais d'annulation et conditions générales des services de prestataires tiers
Lors de la réservation de services auprès de fournisseurs tiers, par exemple des réservations de vol, de location de voiture ou d'hôtel, les conditions générales, y compris les frais d'annulation du fournisseur respectif, s'appliquent. Le client a la possibilité de consulter les conditions générales du fournisseur tiers dans nos locaux commerciaux. Sur demande, nous vous enverrons les conditions générales correspondantes.
### 5. Frais supplémentaires
Les frais supplémentaires tels que l'électricité, le gaz, l'eau et le nettoyage final sont inclus dans le loyer. Si les bouteilles de gaz existantes viennent à manquer, les propriétaires ou la personne de contact concernée doivent être immédiatement informés. Les frais de chauffage encourus seront facturés en fonction de la consommation.
### 6. Draps et serviettes
Tous les lits sont fraîchement faits à l'arrivée. Le linge de lit peut être changé une fois par semaine. Il est déjà dans les placards ou peut être demandé au propriétaire ou à la personne de contact. Les serviettes de cuisine et de salle de bain sont également fournies et peuvent être changées au moins deux fois par semaine. Cependant, les serviettes ne peuvent pas être emportées à la plage.
### 7. Changement de logement
Si, pour des raisons imprévisibles, un logement de vacances loué n'est plus habitable, un bien équivalent en termes d'équipement et d'emplacement sera mis à disposition sur place. Il n'y a pas droit à une réduction pour la période d'hébergement dans un logement alternatif équivalent.
### 8. Changements de prix
Les prix des locations de vacances de notre catalogue peuvent évoluer dans le temps. Cependant, une fois la réservation confirmée et l'enregistrement du voyage terminé, les prix indiqués sont fermes. Lors de la réservation de vols, de voitures de location et d'hôtels, les informations sur les prix sont soumises aux éventuelles fluctuations de prix du voyagiste concerné.

### 9. Les plans d'étage dans notre catalogue

Les plans des différentes maisons de vacances ou appartements de notre catalogue ne sont pas à l'échelle. Ils ne servent qu'à se faire une idée de la disposition des lieux.

### 10. Réclamations
Toutes les plaintes liées à la vie quotidienne dans ces villages portugais, par exemple les aboiements de chiens dans le voisinage ou le tintement des cloches des églises, ne peuvent être prises en compte par nous.

### 11. Lacunes dans les logements
Nos clients sont tenus d'informer immédiatement le propriétaire ou la personne de contact concernée de tout défaut de leur logement afin qu'il puisse y être remédié en temps utile. Si les défauts ne peuvent être réparés et s'ils sont si importants qu'un séjour supplémentaire n'est pas raisonnable, notre interlocuteur s'efforcera de fournir un objet d'équipement et d'emplacement égaux sur place. Si le voyageur omet coupable de signaler un défaut sur place, aucune réduction du prix de la location ne pourra être réclamée.

### 12. Délai de notification des défauts
Les réclamations pour la fourniture non contractuelle du voyage doivent être revendiquées par nos clients directement auprès de nous dans un délai d'un mois à compter de la fin du voyage convenue contractuellement. Aux fins de preuve, il est recommandé de faire valoir les réclamations par écrit. Après l'expiration de ce délai, la réclamation ne peut être revendiquée que si nos clients ont été empêchés de respecter le délai sans faute de leur part. Les réclamations du client des §§ 651 c - 651 f BGB expirent après un an. Le délai de prescription commence le jour où le voyage convenu contractuellement s'est terminé ou devrait se terminer. Si des négociations sont en cours entre le client et le prestataire de voyage au sujet de la réclamation ou des circonstances justifiant la réclamation, le délai de prescription est suspendu jusqu'à ce que le client ou le prestataire de voyage refuse de poursuivre la négociation. Le délai de prescription court au plus tôt trois mois après la fin de la suspension. Les actions fondées sur la responsabilité délictuelle se prescrivent après trois ans.

### 13. Panne d'électricité et d'eau
Dans ces villages portugais, il peut arriver en été que l'eau s'épuise et soit complètement coupée pendant quelques heures. Dans de rares cas, il y a aussi une panne de courant en cas de fortes pluies ou de vent. Les habitants du village doivent composer avec ces circonstances tout comme nos clients. Dans aucun de ces cas, des droits de réduction ne peuvent être revendiqués.

### 14. Responsabilité
La responsabilité pour les dommages autres que corporels est limitée à trois fois le prix d'occupation, sauf si nous avons causé le dommage intentionnellement ou par négligence grave. Si nous agissons en tant qu'intermédiaire pour d'autres prestataires de services, les prestataires concernés sont responsables, à moins que nous n'ayons manqué de manière coupable à nos obligations d'information, de fourniture d'informations ou de diligence.

### 15. Résiliation du contrat en raison de circonstances exceptionnelles
Si le voyage est rendu considérablement plus difficile, mis en danger ou compromis en raison d'un cas de force majeure qui n'était pas prévisible au moment de la conclusion du contrat, tel qu'un tremblement de terre, une guerre, une guerre civile ou une épidémie, nous et nos clients pouvons résilier le contrat. Si le contrat est résilié, nous exigeons un paiement proportionnel du prix total pour les services déjà fournis. Si le voyage n'a pas encore commencé, toute réclamation de nos clients sera nulle.

### 16. Services non utilisés
Si nos clients n'utilisent pas les prestations de voyage individuelles, notamment en raison d'une arrivée tardive et/ou d'un retour anticipé ou pour d'autres raisons impérieuses, aucun remboursement ne pourra être revendiqué. Cependant, si nous parvenons à louer le logement en question à un autre client à court terme, nous rembourserons les sommes que nous obtenons d'une autre location.

### 17. Responsabilité et obligations particulières du client
La propriété réservée ne peut être occupée que par le nombre de personnes contractuellement convenu. En cas de surpeuplement, nous sommes en droit soit d'exiger une indemnisation appropriée pour la période de surpeuplement, soit d'exiger que les personnes excédentaires quittent immédiatement le bien loué. Cette demande peut également être faite par la personne de contact sur place. Les animaux domestiques ne peuvent être amenés qu'avec l'autorisation préalable. Nos clients sont tenus de traiter le bien loué avec soin et de signaler immédiatement tout dommage ou défaut. Après le départ, la propriété doit être remise dans un état propre. Toute la vaisselle doit être lavée et les déchets doivent être jetés. Si, après le départ, un nettoyage final qui dépasse le niveau habituel, ou si des dommages à l'équipement de la propriété réservée sont constatés, nous sommes en droit de facturer à nos clients les frais engagés au titre des dommages. Nos clients sont responsables des dommages qu'ils causent de manière fautive au logement loué pendant leur séjour. Les propriétaires peuvent exiger qu'une liste d'inventaire soit signée par les clients sur place. S'il manque quelque chose à la fin du séjour, nos clients sont tenus de réparer le dommage s'ils sont responsables de la perte.

### 18. Assurance annulation de voyage
Dans tous les cas, nous vous recommandons de souscrire une assurance annulation de voyage.

### 19. Clause de sauvegarde
Si l'un des règlements ci-dessus est inefficace, les règlements restants restent valables.
