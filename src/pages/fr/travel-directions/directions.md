---
tags: travelDirections
partial: directions
---

## Informations générales

L'aéroport le plus proche est à Lisbonne, à environ 65 km au sud de nous. De là, vous pouvez prendre le bus express directement à Praia da Areia Branca, très confortablement et à peu de frais, où nous serons heureux de venir vous chercher à l'arrêt de bus. Sinon, vous pouvez également louer une voiture à l'aéroport, arriver en taxi ou utiliser notre transfert aéroport. L'arrivée au surf camp est possible le dimanche à partir de 16h et le départ jusqu'à 11h. Avant et après, vous êtes bien sûr les bienvenus pour stocker vos bagages avec nous et profiter de votre temps avec nous.

## bus express

Le bus express part de la gare routière "Sete Rios". Pour vous rendre à la gare routière, vous pouvez soit prendre le métro, Uber ou un taxi.

**Métro :** Après votre arrivée, sortez de l'aéroport et tournez à droite en direction du Métro. Au distributeur de billets, vous achetez un billet rechargeable (50 cents) par personne avec un voyage (1,10 EUR). Prenez ensuite la ligne rouge "Vermelho" jusqu'à "Sao Sebastiao", changez pour la ligne bleue "Azul" en direction de "Reboleira" et descendez à l'arrêt "Jardim Zoologico".

**Uber :** Après votre arrivée (EG) vous devez vous rendre au "Passenger Drop Off" devant le hall de départ (OG) car Uber n'est autorisé à s'arrêter qu'ici. Ici, vous pouvez prendre un Uber jusqu'à la gare routière ou le faire conduire directement au camp de surf.

**Taxi :** Ici, il faut faire attention à ne pas se faire avoir. Le trajet ne devrait pas coûter plus de 10 EUR. Un conseil : ne prenez pas les taxis qui attendent aux arrivées, mais rendez-vous aux départs et prenez là-bas un taxi qui amenait quelqu'un à l'aéroport.

Arrivé à la gare routière "Sete Rios", vous devez acheter un billet pour Praia da Areia Branca au guichet avant de monter dans le bus express avec le terminus "Peniche". Vous pouvez également acheter les billets en ligne sur ce lien ou télécharger l'application très pratique avec laquelle vous pouvez enregistrer les billets de bus sur votre smartphone : <a href="https://www.rede-expressos.pt/" target="_blank ">rede-expressos.pt</a>

Le trajet en bus express de Lisbonne à Praia da Areia Branca coûte 8,90 EUR et dure exactement 1 heure et 15 minutes. Le terminus est Peniche. Praia da Areia Branca est l'avant-dernier arrêt et non une gare routière, juste un petit arrêt de bus discret au milieu de Praia da Areia Branca. Si vous avez traversé le chef-lieu de district de Lourinha, vous devez descendre au prochain arrêt.

La meilleure chose à faire est d'essayer de prendre le bus qui arrive à Praia da Areia Branca à 17h00 ou 18h15. Alors vous êtes juste à temps pour le barbecue de bienvenue, qui a lieu ici tous les dimanches.

## Transfert de l'aéroport

Si vous souhaitez un service personnalisé, nous serons heureux de venir vous chercher à tout moment à l'aéroport de Lisbonne ou de vous y emmener. Ceci est particulièrement utile si votre vol a lieu en dehors des horaires de trajet des bus express. Nous vous accueillons dans la zone des arrivées au Starbucks. Vous pouvez nous reconnaître par le T-shirt Da Silva. Nous sommes heureux de nous arrêter dans un supermarché sur le chemin du surf camp afin que vous puissiez faire le plein des choses les plus importantes pour les premiers jours. Notre navette peut accueillir jusqu'à 8 personnes. Assurez-vous de vous enregistrer et de confirmer le transfert aéroport avec nous en temps utile.

```
Prix ​​pour 1 - 4 personnes = 80 EUR
Prix ​​pour 5 - 8 personnes = 120 EUR
+ Supplément nuit de 20 EUR (de 01h00 à 05h00)
```

## Arriver en voiture

Si vous venez en voiture, utilisez le **Dispositif de navigation : NE PAS aller à l'adresse postale** ! Il y a **plusieurs rues avec le même nom** dans notre région et il est donc souvent navigué à la mauvaise adresse. Le moyen le plus sûr de nous trouver est sur **Google-Maps : "Da Silva Surfcamp"**.

## Petite escale à Lisbonne?

Souhaitez-vous découvrir la belle ville de Lisbonne avant ou après votre séjour chez nous? Ensuite, nous pouvons absolument vous recommander l'auberge <a href="https://www.lisboncalling.net/" target="_blank">Lisbon Calling</a>. Il est très bien situé, à seulement 3 minutes à pied du quartier le plus branché "Cais Sodré" et à seulement 10 minutes à pied du "Bairro Alto", où il y a un pub sympa à côté de l'autre. il y a extrêmement agréable et l'auberge est vraiment joliment meublée. Il est préférable de jeter un œil à la page d'accueil et de se faire sa propre impression :-)

Et si vous le voulez un peu plus calme et avec votre propre salle de bain, vous êtes entre de meilleures mains dans la nouvelle hébergement <a href="https://www.lisboncalling.net/double-rooms-studio/" target="_blank">Lisbon Calling dans le Rua da Fé</a>.
