const generateImages = require("./images")

async function renderBookingTeasers({
  links,
  teasers
}) {
  if (!teasers || !teasers.length) return ""
  return `
    <div class="booking-teasers">
      <div class="booking-teasers__container">
        ${await renderTeasers(links, teasers)}
      </div>
    </div>
  `
}

async function renderTeasers(linksData, teasers) {
  const items = teasers.map(async (teaser) => {

    const image = await generateImages({
      src: linksData[teaser.link].image,
      alt: linksData[teaser.link].title,
      myClass: 'gallery-image'
    })

    return `
      <div class="booking-teasers__teaser">
        <a href="${linksData[teaser.link].path}">
          ${image}
        </a>
      </div>
    `
  })
  
  return (await Promise.all(items)).join(' ')
}

module.exports = renderBookingTeasers
