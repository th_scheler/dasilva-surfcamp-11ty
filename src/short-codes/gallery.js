const generateImages = require('./images')

async function renderGallery({ images, altDefault, maxDisplayItems }) {

  if (!Array.isArray(images) || !images.length) return

  const _images = images.map(i => {
    return {
      src: i.src || '/_assets/logos/dasilva-surfcamp-logo.png',
      alt: i.alt || altDefault || 'photo',
      link: i.link || ''
    }
  })
  const _maxDisplayItems = maxDisplayItems || 9

  return `<div class="gallery show-more-button">
      ${(await renderImageGrid(_images, _maxDisplayItems)).join(" ")}
    </div>`
}

async function renderImageGrid(images, maxDisplayItems) {
  const grid = images.map(async (image, index) => {
    return `
      ${renderDivAndMoreButton(images.length, index, maxDisplayItems, image.src)}
        ${renderLink(image.link, image.src)}
          ${await renderImage(image.src, image.alt)}
        </a>
      </div>
      ${await renderGalleryPopup(images, index, image.src, image.alt)}
    `
  })
  return await Promise.all(grid)
}

function renderDivAndMoreButton(itemsTotal, index, maxDisplayItems, imageSource) {
  let div = ''
  if ((index < maxDisplayItems -1) || (itemsTotal === maxDisplayItems -1)) {
    div = `<div>`
  } else if (index === maxDisplayItems -1) {
    div = `<div class="gallery__last-item-maxdisplay">
      <div class="gallery__more-button">
        <img src="/_assets/icons/svgrepo/pictures.svg" 
        class="gallery__more-button-icon" alt="more pictures"
        onclick="document.getElementById('${imageSource}').classList.add('gallery-popup__show')">
      </div>`
  } else {
    div = `<div class="gallery__more-than-maxdisplay">`
  }
  return div
}

function renderLink(link, source) {
  if (link) {
    return `<a href="${link}">`
  } else {
    return `<a href="javascript:(void(0))" 
      onclick="document.getElementById('${source}').classList.add('gallery-popup__show')">`
  }
}

async function renderGalleryPopup(images, index, source, alt) {
  const prev = index > 0 ? images[index -1].src : images[images.length -1].src
  const next = index < images.length -1 ? images[index +1].src : images[0].src
  return ` 
    <div class="gallery-popup" id="${source}">
      <img src="/_assets/icons/svgrepo/close.svg" 
        class="gallery-popup__control-icon gallery-popup__close" alt="close"
        onclick="document.getElementById('${source}').classList.remove('gallery-popup__show')">
      <div class="gallery-popup__button-fill gallery-popup__control-icon gallery-popup__close">&nbsp;</div>
      <img src="/_assets/icons/svgrepo/previous.svg"
        class="gallery-popup__control-icon gallery-popup__previous" alt="previous"
        onclick="document.getElementById('${prev}').classList.add('gallery-popup__show');
        document.getElementById('${source}').classList.remove('gallery-popup__show')">
      <div class="gallery-popup__button-fill gallery-popup__control-icon gallery-popup__previous">&nbsp;</div>
      ${await renderImage(source, alt)}
      <img src="/_assets/icons/svgrepo/next.svg" 
        class="gallery-popup__control-icon gallery-popup__next" alt="next"
        onclick="document.getElementById('${next}').classList.add('gallery-popup__show');
        document.getElementById('${source}').classList.remove('gallery-popup__show')">
      <div class="gallery-popup__button-fill gallery-popup__control-icon gallery-popup__next">&nbsp;</div>
    </div>
  `
}

async function renderImage(source, alt) {
  const imageOrig = source.replace(/\/_assets/, './src/assets')
  return generateImages({
    src: imageOrig,
    alt: alt,
    myClass: 'gallery-image'
  })
}


module.exports = renderGallery