const generateImages = require("./images")

async function renderSurroundingsCards({
  cards
}) {
  if (!cards || !cards.length) return ""

  return `
    <div class="surroundings-cards">
      <div class="surroundings-cards__container">
        ${await renderCards(cards)}
      </div>
    </div>
  `
}

async function renderCards(cards) {
  const items = cards.map(async (card) => {

    const cardTitle = card.title
      ? `<h3 class="surroundings-cards__title">${card.title}</h3>`
      : ''
  
    const image = card.imageSrc 
      ? await generateImages({
          src: card.imageSrc,
          alt: card.alt || card.title || 'surroundings',
          myClass: 'surroundings-cards__image'
        })
      : ''
    
    const cardMapLink = card.mapLink && card.mapLink.trim()
        ? `<a href="${sanitizeLink(card.mapLink)}" target="_blank"><img src="/_assets/icons/svgrepo/google-maps.svg" class="surroundings-cards__map-link" alt="google maps link"></a>`
        : ''

    const cardWebLink = card.webLink && card.webLink.trim()
        ? `<a href="${sanitizeLink(card.webLink)}" target="_blank"><img src="/_assets/icons/svgrepo/web-link.svg" class="surroundings-cards__web-link" alt="web link"></a>`
        : ''

    return `
      <div class="surroundings-cards__card">
        ${cardTitle}
        <div>
          ${image}
        </div>
        <div>
          ${card.text || ''}
          ${cardMapLink}
          ${cardWebLink}
        </div>
      </div>
    `
  })
  
  return (await Promise.all(items)).join(' ')
}

function sanitizeLink(link) {
  // prefix https:// if not exsiting
  return link.trim().replace(/^(https:\/\/)*/, 'https://')
}
module.exports = renderSurroundingsCards
